package services.adaptors

import com.rabbitmq.http.client.Client

trait RabbitMQAPIAdaptor {
  def createClient(url:String, username:String, password:String): Client
}
