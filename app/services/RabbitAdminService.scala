package services

import models.BindingType.BindingType
import models.{Project, ProjectBindings}

import scala.concurrent.Future
import scala.util.Try

final case class RabbitAdminServiceException(private val message: String = "",
                                             private val cause: Throwable = None.orNull) extends Exception(message, cause)

trait RabbitAdminService {
  @throws[RabbitAdminServiceException]("If there is a problem with RabbitMQ Admin Service")
  def getProjectBindings(rabbitMQURI: String, vhost: String,
                         project: Project): Future[ProjectBindings]

  @throws[RabbitAdminServiceException]("If there is a problem with RabbitMQ Admin Service")
  def rebindProjectToQueue(rabbitMQURI: String, vhost: String, project: Project,
                           destQueue: String, newBindings: Map[String, models.Process],
                           bindingType: BindingType): Future[Try[String]]
}
