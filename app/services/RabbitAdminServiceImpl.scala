package services

import _root_.services.adaptors.RabbitMQAPIAdaptor
import com.rabbitmq.http.client.Client
import models.BindingType.BindingType
import models._

import scala.collection.JavaConverters._
import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

/**
  * This class manages the connection to the RabbitMQ's Admin API
  *
  * @param adaptor
  */
class RabbitAdminServiceImpl(adaptor: RabbitMQAPIAdaptor) extends RabbitAdminService {
  // Cache of connections by URI
  val clientCache: TrieMap[String, Client] = new TrieMap[String, Client]()

  // Use the cache to conserve connections to the RabbitMQ Admin API
  private def getClient(rabbitMQURI: String): Client = {
    val (url, username, password) = ClusterConfig.parseURI(rabbitMQURI)

    // Get a client from the cache or create a new one if this is our first time to talk to this RabbitMQ Admin Host
    clientCache.getOrElseUpdate(rabbitMQURI, adaptor.createClient(url + "/api/", username, password))
  }

  /**
    * Retreive all of the bindings between the exchange represented by the project
    *
    * @param rabbitMQURI
    * @param vhost
    * @param project
    * @throws RabbitAdminServiceException if there is a problem connecting to the API
    * @return List of bindings across all queues
    */
  @throws[RabbitAdminServiceException]("If there is a problem with RabbitMQ Admin Service")
  def getProjectBindings(rabbitMQURI: String, vhost: String, project: Project): Future[ProjectBindings] = {

    Future {
      val client = getClient(rabbitMQURI)

      val bindingRecs = client.getBindingsBySource(vhost, project.name).asScala.toList
      val bindingMap = bindingRecs.map(bindingRec => new ToolBinding(bindingRec.getDestination, bindingRec.getRoutingKey))
        .groupBy(_.destination)

      new ProjectBindings(project, bindingMap)
    }
  }

  /**
    * Unbinds the queue from the exchange. Expects the list of existing bindings.
    * Note that if a single unbind operation fails, it will leave the remaining bindings in place
    * and neither roll back the operation nor continue.
    *
    * @param rabbitMQURI
    * @param vhost
    * @param project
    * @param destQueue
    * @param existingBindings
    * @return Basic Sucess/Failure - there is no information inside the success instance returned
    */
  def unbindQueueFromProject(rabbitMQURI: String, vhost: String, project: Project, destQueue: String,
                             existingBindings: ProjectBindings): Future[Try[String]] = {
    Future {
      val client = getClient(rabbitMQURI)
      try {
        existingBindings.bindings(destQueue).map(binding =>
          client.unbindQueue(vhost, destQueue, project.name, binding.routingKey)
        )
        Success("Ok")
      } catch {
        case e: Exception => Failure(e)
      }
    }
  }

  private def mapMimeTypeToBinding(processType:String, mimeType:String): String = {
    val bindStr = mimeType.replace('/','.').replace('*','#')
    s"*.$processType.$bindStr"
  }
  /**
    * Accepts a list of binding keys and calls the Rabbit Admin API to bind each of them to the given exchange
    *
    * @param rabbitMQURI
    * @param vhost
    * @param project
    * @param destQueue
    * @param newBindings List of binding keys to apply
    * @throws RabbitAdminServiceException
    * @return
    */
  @throws[RabbitAdminServiceException]("If there is a problem with RabbitMQ Admin Service")
  private def bindProjectToQueue(rabbitMQURI: String, vhost: String, project: Project,
                                 destQueue: String, newBindings: Map[String, models.Process]): Future[Unit] = {
    Future {
      val client = getClient(rabbitMQURI)
      try {
        newBindings.keys.map(processKey => {
          newBindings(processKey).mimeTypes.map(binding =>
          client.bindQueue(vhost, destQueue, project.name, mapMimeTypeToBinding(processKey,binding)))
        })
      } catch {
        case e: Exception => throw new RabbitAdminServiceException("Can't bind file messages to " + destQueue, e)
      }
    }
  }

  /**
    * Create a binding for tool messages. This key is formated as 'extractors.<queue name >
    *
    * @param rabbitMQURI
    * @param vhost
    * @param project
    * @param destQueue
    * @throws RabbitAdminServiceException if there are communications problems with the API server
    * @return
    */
  @throws[RabbitAdminServiceException]("If there is a problem with RabbitMQ Admin Service")
  private def bindToolMessagesToProject(rabbitMQURI: String, vhost: String, project: Project,
                                        destQueue: String): Future[Unit] = {
    Future {
      val client = getClient(rabbitMQURI)
      try {
        client.bindQueue(vhost, destQueue, project.name, s"extractors.$destQueue")
      } catch {
        case e: Exception => throw new RabbitAdminServiceException("Can't bind tool messages to " + destQueue, e)
      }
    }
  }


  /**
    * Helper method to do the actual bindings for the "All Messages" binding type. Calls method to bind all
    * of the provided binding keys and then makes the call to bind tool messages
    *
    * @param rabbitMQURI
    * @param vhost
    * @param project
    * @param destQueue
    * @param newBindings
    * @return Basic Sucess/Failure - there is no information inside the success instance returned
    */
  private def bindAllMessages(rabbitMQURI: String, vhost: String, project: Project,
                              destQueue: String, newBindings: Map[String, models.Process]): Future[Try[String]] = {
    (for {
      _ <- bindProjectToQueue(rabbitMQURI, vhost, project, destQueue, newBindings)
      _ <- bindToolMessagesToProject(rabbitMQURI, vhost, project, destQueue)
    } yield Success("Ok")
      ) recover {
      case RabbitAdminServiceException(s, e) => Failure(e)
    }
  }

  /**
    * Helper method to do the actual bindings for the "Tool Messages only" binding type.
    *
    * @param rabbitMQURI
    * @param vhost
    * @param project
    * @param destQueue
    * @return
    */
  private def bindToolMessages(rabbitMQURI: String, vhost: String, project: Project,
                               destQueue: String): Future[Try[String]] = {
    (for {
      _ <- bindToolMessagesToProject(rabbitMQURI, vhost, project, destQueue)
    } yield Success("Ok")
      ) recover {
      case RabbitAdminServiceException(s, e) => Failure(e) // Turn future exceptions into failure
    }
  }


  /**
    * Update the bindings between an exchange and a particular queue. It does this by
    * first removing all of the bindings for that queue and then re-applying the new ones
    *
    * @param rabbitMQURI
    * @param vhost
    * @param project
    * @param destQueue
    * @param newBindings
    * @param bindingType
    * @return
    */
  @Override
  def rebindProjectToQueue(rabbitMQURI: String, vhost: String, project: Project,
                           destQueue: String, newBindings: Map[String, models.Process],
                           bindingType: BindingType): Future[Try[String]] = {

    (for {
      existingBindings <- getProjectBindings(rabbitMQURI, vhost, project)

      // Reverse any existing bindings, or skip if there are no bindings at all for this queue
      unbindResult <- if (existingBindings.bindings.contains(destQueue))
        unbindQueueFromProject(rabbitMQURI, vhost, project, destQueue, existingBindings)
      else Future(Success("Ok"))
    } yield {

      // Only create new bindings if the unbind worked
      unbindResult match {
        case Success(_) => {
          bindingType match {
            case BindingType.AllMessages =>
              bindAllMessages(rabbitMQURI, vhost, project, destQueue, newBindings)


            case BindingType.ToolMessagesOnly =>
              bindToolMessages(rabbitMQURI, vhost, project, destQueue)

            //Don't do any bindings if that is the new setting
            case BindingType.None =>
              Future(Success("Ok"))
          }
        }
        case Failure(f) => Future(Failure(f)) // Unbind failed
      }
    }) flatten // Eliminate Future[Future
  }
}
