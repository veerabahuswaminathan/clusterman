package services

import models.{ExtractorInfo, ToolsCatalogRecord}
import play.api.Logger
import play.api.libs.json.JsResultException
import play.api.libs.ws.WSClient

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ToolsCatalogService {

  /**
    * Query the Tools Catalog to retrieve the extractor info blob for the reqeusted tool ID
    * @param ws
    * @param toolsCatalogURI
    * @param toolId
    * @return
    */
  def getExtractorInfo(ws: WSClient, toolsCatalogURI: String, toolId: String): Future[Option[ExtractorInfo]] = {
    val responseFut = ws.url(s"$toolsCatalogURI/api/v1/toolVersions/$toolId/extractorConfig").get()
    responseFut.map(response => {
      response.status match {
        case 200 =>{
          try {
            Some(response.json.as[ExtractorInfo])
          } catch {
            case e: JsResultException => {
              Logger.error("Can't parse Extractor_Info record", e)
              None
            }
          }
        }
        case _ => {
          Logger.error("cannot get results from URL " + toolsCatalogURI + "/api/v1/toolVersion/" + toolId +
            "/extractorConfig: "+ response.statusText)
          None
        }
      }
    }
    )
  }

  /**
    * Get the full description of the selected tool from the tools catalog. The result will be an object
    * that contains both the tool and the toolVersion
    * @param ws
    * @param toolsCatalogURI
    * @param toolId
    * @return
    */
  def getTool(ws: WSClient, toolsCatalogURI: String, toolId: String): Future[Option[ToolsCatalogRecord]] = {
    val responseFut = ws.url(toolsCatalogURI + "/api/v1/toolVersions/" + toolId).get()
    responseFut.map(response => {
      response.status match {
        case 200 =>{
          try {
            Some(response.json.as[ToolsCatalogRecord])
          } catch {
            case e: JsResultException => {
              Logger.error("Can't parse tools catalog record", e)
              None
            }
          }
        }
        case _ => {
          Logger.error("cannot get results from URL " + toolsCatalogURI + "/api/v1/toolVersions/" + toolId + ": "+ response.statusText)
          None
        }
      }
    }
    )
  }

  /**
    * Request a list of all of the extractors in the tools catalog
    * @param ws
    * @param toolsCatalogURI
    * @return
    */
  def getExtractors(ws: WSClient, toolsCatalogURI: String): Future[List[ToolsCatalogRecord]] = {
    getRecords(ws, toolsCatalogURI, "extractors")
  }

  /**
    * Request a list of all of the converters in the tools catalog
    * @param ws
    * @param toolsCatalogURI
    * @return
    */
  def getConverters(ws: WSClient, toolsCatalogURI: String): Future[List[ToolsCatalogRecord]] = {
    getRecords(ws, toolsCatalogURI, "converters")
  }

  private def getRecords(ws: WSClient, toolsCatalogURI: String, recordType: String): Future[List[ToolsCatalogRecord]] ={
    val responseFut = ws.url(toolsCatalogURI + "/api/v1/" + recordType).get()
    responseFut.map(response => {
      response.status match {
        case 200 =>{
          try {
            response.json.as[List[ToolsCatalogRecord]]
          } catch {
            case e: JsResultException => {
              e.printStackTrace()
              Logger.error("Can't parse tools catalog record", e)
              List[ToolsCatalogRecord]()
            }
          }
        }
        case _ => {
          Logger.error("cannot get results from URL " + toolsCatalogURI + "/api/v1/" + recordType + ": "+ response.statusText)
          List[ToolsCatalogRecord]()
        }
      }
    }
    )
  }
}
