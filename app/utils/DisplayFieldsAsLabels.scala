package utils

import views.html

object DisplayFieldsAsLabels {
  import views.html.helper.FieldConstructor
  implicit val myFields = FieldConstructor(html.displayFieldsAsLabels.f)
}
