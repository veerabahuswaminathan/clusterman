package utils

import com.mohiva.play.silhouette.api.Silhouette
import controllers.AssetsFinder
import play.api.http.HttpErrorHandler
import play.api.mvc._
import play.api.mvc.Results._

import scala.concurrent._
import javax.inject.{Inject, Singleton}

import models.ClusterConfigs
import models.daos.ProjectDAO
import org.webjars.play.WebJarsUtil
import play.api.{Configuration, Logger}
import play.api.i18n.I18nSupport
import play.api.libs.ws.WSClient
import services.{ClusterstatsService, RabbitAdminService, ToolsCatalogService}
import utils.auth.DefaultEnv

@Singleton
class ErrorHandler extends HttpErrorHandler  {

  def onClientError(request: RequestHeader, statusCode: Int, message: String) = {
    Future.successful(
      Status(statusCode)("A client error occurred: " + message + " for "+request.toString())
    )
  }

  def onServerError(request: RequestHeader, exception: Throwable) = {
    Logger.error("Error rendering page "+request+" - "+exception.getMessage)
    exception.printStackTrace()
    Future.successful(
      InternalServerError(views.html.errorPage(exception.getMessage))
    )
  }
}