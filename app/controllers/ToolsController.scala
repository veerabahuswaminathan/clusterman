package controllers

import javax.inject.Inject

import _root_.services.{ClusterstatsService, RabbitAdminService, ToolsCatalogService}
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import models._
import models.daos.ProjectDAO
import org.webjars.play.WebJarsUtil
import play.api.Configuration
import play.api.Logger
import play.api.i18n.I18nSupport
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents}
import play.api.libs.json._
import play.api.libs.json.Json._
import utils.auth.DefaultEnv

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class ToolsController @Inject()(
                                    components: ControllerComponents,
                                    silhouette: Silhouette[DefaultEnv],
                                    config: Configuration,
                                    clusterConfigs: ClusterConfigs,
                                    projectDAO: ProjectDAO,
                                    clusterStatsService: ClusterstatsService,
                                    rabbitAdminService: RabbitAdminService,
                                    toolsCatalogService: ToolsCatalogService,
                                    ws: WSClient
                                  )(
                                    implicit
                                    webJarsUtil: WebJarsUtil,
                                    assets: AssetsFinder
                                  ) extends AbstractController(components) with I18nSupport {

  implicit val clusters: ClusterConfigs = clusterConfigs

  /**
    * First step of the deploy process.
    * Obtain the full list of extractors and converters and pass into the page to populate the chooser
    *
    * @param clusterName
    * @return The result to display.
    */
  def chooseTool(clusterName: String) = silhouette.SecuredAction.async { implicit request: SecuredRequest[DefaultEnv, AnyContent] => {
    val TCUrl = config.get[String]("toolscatalog.uri")
    for {
      extractors <- toolsCatalogService.getExtractors(ws, TCUrl)
      converters <- toolsCatalogService.getConverters(ws, TCUrl)
    } yield {
      (Ok(views.html.chooseTool(request.identity, clusterName, extractors, converters)))
    }
  }
  }

  /**
    * Second step of the deploy process.
    * Fetch data from tools catalog and populate the form object which the user will edit in this step.
    * There are a few hidden fields which are stored in this form to avoid having to query the tool again for
    * the third step.
    * @param clusterName
    * @param toolId
    * @return
    */
  def enterDeployParameters(clusterName: String, toolId: String) = silhouette.SecuredAction.async { implicit request: SecuredRequest[DefaultEnv, AnyContent] => {
    val toolsCatalogURI = config.get[String]("toolscatalog.uri")
    for {
      toolInfoOpt <- toolsCatalogService.getTool(ws, toolsCatalogURI, toolId)
    }
      yield {
        toolInfoOpt match {
          case Some(toolInfo) => {
            toolInfo.params match{
              case Some(params) => {

                if(params.dockerimageName.isDefined) {
                  val deployFormData = new DeploymentFormData(
                    toolInfo.version,
                    1, 1, 32, 2,
                    toolInfo.toolType,
                    ToolsCatalogRecord.getQueueName(toolInfo),
                    toolInfo.params.get.dockerimageName.get,
                    toolInfo.toolId)

                  val form = DeploymentFormData.deploymentForm.fill(
                    deployFormData
                  )

                  Ok(views.html.deploymentInfoForm(request.identity, clusterName, toolId, form))
                } else{
                  BadRequest("Tool Record is missing docker image name. ID "+toolId)
                }
              }
              case _ => {
                BadRequest("Tool Record is missing required properties. ID "+toolId)
              }
            }
          }
          case None => {
            BadRequest("Invalid tool description in tools catalog for toolVersion ID "+toolId)
          }
        }
      }
  }
  }

  def deployToolToCluster(clusterName: String, toolId: String) = silhouette.SecuredAction.async { implicit request: SecuredRequest[DefaultEnv, AnyContent] => {
    DeploymentFormData.deploymentForm.bindFromRequest.fold(
      formWithErrors => {
        println(formWithErrors.errors)
        Future(BadRequest("Invalid data"))
      },
      bindingForm => {
        val clusterConfigOpt = ClusterConfigs.getConfig(clusterConfigs, clusterName)

        clusterConfigOpt match {
          case Some(clusterConfig) => {
            val deployParams = DeploymentFormData.toDeploymentParms(bindingForm, clusterConfig)
            println("Going to deploy  "+deployParams)
            clusterStatsService.deploy(ws, clusterConfig.swarmURI, deployParams) map {rslt =>
              rslt match{
                case Success(rsltString) => Ok(s"Operation sucess in ${bindingForm.serviceName}cluster $clusterName   "+rslt)
                case Failure(eek) => InternalServerError("Can't deploy to cluster "+eek.getMessage)
              }
            }
          }

          case _ => Future(BadRequest("Can't find cluster config for "+clusterName))
      }
      }
    )
  }
  }
}