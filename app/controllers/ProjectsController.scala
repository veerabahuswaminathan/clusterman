package controllers

import javax.inject.Inject

import _root_.services.{ClusterstatsService, RabbitAdminService, ToolsCatalogService}
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import models._
import models.daos.ProjectDAO
import org.webjars.play.WebJarsUtil
import play.api.Configuration
import play.api.i18n.I18nSupport
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents}
import utils.auth.DefaultEnv

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class ProjectsController @Inject()(
                                    components: ControllerComponents,
                                    silhouette: Silhouette[DefaultEnv],
                                    config: Configuration,
                                    clusterConfigs: ClusterConfigs,
                                    projectDAO: ProjectDAO,
                                    clusterStatsService: ClusterstatsService,
                                    rabbitAdminService: RabbitAdminService,
                                    toolsCatalogService: ToolsCatalogService,
                                    ws: WSClient
                                  )(
                                    implicit
                                    webJarsUtil: WebJarsUtil,
                                    assets: AssetsFinder
                                  ) extends AbstractController(components) with I18nSupport {

  implicit val clusters: ClusterConfigs = clusterConfigs

  /**
    * Handles the showBindings action.
    *
    * @return The result to display.
    */
  def showBindingsForProject(clusterName: String, projectName: String) = silhouette.SecuredAction.async { implicit request: SecuredRequest[DefaultEnv, AnyContent] => {
    val tryProcess =
      for {
        clusterConfig <- ClusterConfigs.getConfig(clusterConfigs, clusterName);
        extractorRabbitConfig <- ClusterConfig.getRabbitMQEntry(clusterConfig, ToolType.Extractor.toString)
      } yield {

        for {
          extractors <- clusterStatsService.getExtractorsForVhost(ws, clusterConfig.swarmURI, clusterConfig.vhost)
          projectOpt <- projectDAO.getProjectByName(clusterName, projectName)
          bindings <- rabbitAdminService.getProjectBindings(extractorRabbitConfig.rabbitMQURI, clusterConfig.vhost, projectOpt.get) if projectOpt.isDefined
        } yield {

          val form = BindingFormData.bindingForm.fill(
            BindingFormData.generateFormDataForDeployedTools(extractors, bindings)
          )

          Ok(views.html.bindTools(request.identity, clusterName, projectName, extractors, bindings, form))
        }
      }

    tryProcess match {
      case Some(result) => result
      case None => Future(BadRequest("Invalid cluster"))
    }
  }
  }

  private def updateBindings(clusterName:String, projectName:String, updatedBinding:BindingFormDetail):Future[Try[String]] = {

    val resultOpt = for {
      clusterConfig <- ClusterConfigs.getConfig(clusterConfigs, clusterName);
      extractorRabbitConfig <- ClusterConfig.getRabbitMQEntry(clusterConfig, ToolType.Extractor.toString)
      toolsCatalogURI = config.get[String]("toolscatalog.uri")
    } yield {
      for{
        projectOpt <- projectDAO.getProjectByName(clusterName, projectName)
        extractorDefOpt <- toolsCatalogService.getExtractorInfo(ws, toolsCatalogURI, updatedBinding.toolId)
      } yield {

        if(projectOpt.isDefined && extractorDefOpt.isDefined){
          rabbitAdminService.rebindProjectToQueue(extractorRabbitConfig.rabbitMQURI,
            clusterConfig.vhost,
            projectOpt.get, updatedBinding.queue, extractorDefOpt.get.process, BindingFormData.getBindingTypeAsEnum(updatedBinding))
          Success("OK")
        }
        else
            Failure(new Exception("Can't find extractor info for "+updatedBinding.tool+" ("+updatedBinding.toolId+")"))
        }
      }
    resultOpt.getOrElse(Future(Failure(new Exception("Invalid parameters to request"))))
  }


  /**
    * Operation to process the results from the bindings page
    * @param clusterName
    * @param projectName
    * @return
    */
  def createBindingsForQueue(clusterName: String, projectName: String) = silhouette.SecuredAction.async { implicit request: SecuredRequest[DefaultEnv, AnyContent] => {
    BindingFormData.bindingForm.bindFromRequest.fold(
      formWithErrors => {
        println(formWithErrors.errors)
        Future(BadRequest("Invalid data"))
      },
      bindingForm => {

        // Try to adjust the bindings in the RabbitMQ Admin
        val updateBindingsResultFut = BindingFormData.updatedBindings(bindingForm).map(updatedBinding =>
          updateBindings(clusterName, projectName, updatedBinding))

        // Convert the List of futures into a Future[List]
        val updateBindingsResult = Future.sequence(updateBindingsResultFut)

        // Were there any failures?
        for {
          listOfTrys <- updateBindingsResult
          collectedFailure = listOfTrys.filter(aResult => aResult.isFailure)
        } yield {
          if (collectedFailure.size == 0)
            Redirect(config.get[String]("play.http.context")+"?cluster=" + clusterName)
          else
            BadRequest(s"Operation failed for project $projectName in cluster $clusterName")
        }
      }
    )}
  }
}
