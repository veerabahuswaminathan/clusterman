package modules

import com.google.inject.AbstractModule
import models.ClusterConfigs
import models.daos.{ProjectDAO, ProjectDAOImpl}
import net.codingwell.scalaguice.ScalaModule
import play.api.{Configuration, Environment, Mode}
import reactivemongo.api.{DefaultDB, MongoConnection, MongoDriver}
import services.adaptors.RabbitMQAPIAdaptorImpl
import services.{ClusterstatsService, RabbitAdminService, RabbitAdminServiceImpl, ToolsCatalogService}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Future, _}

class Module(environment: Environment,
              config: Configuration) extends AbstractModule with ScalaModule{
  def configure(): Unit = {
    val clusters = ClusterConfigs.loadFromConfig(config)
    bind[ClusterConfigs].toInstance(clusters)

    if(environment.mode != Mode.Test) {
      val mongoUri = config.get[String]("mongo.uri")
      println("Connecting to " + mongoUri)

      val driver = MongoDriver()
      val parsedUri = MongoConnection.parseURI(mongoUri)
      val connection = parsedUri.map(driver.connection(_))
      val futureConnection = Future.fromTry(connection)
      val dbFuture = futureConnection.flatMap(_.database("clusterman"))

      val db = Await.result(dbFuture, 10 seconds)

      bind[DefaultDB].toInstance(db)
      bind[ProjectDAO].to[ProjectDAOImpl]
      bind[ClusterstatsService].toInstance(new ClusterstatsService())

      val rabbitAdminService = new RabbitAdminServiceImpl(new RabbitMQAPIAdaptorImpl())
      bind[RabbitAdminService].toInstance(rabbitAdminService)

      bind[ToolsCatalogService].toInstance(new ToolsCatalogService())
    }
  }
}

