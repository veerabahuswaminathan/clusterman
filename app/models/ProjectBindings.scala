package models

case class ToolBinding(destination: String, routingKey:String)
case class ProjectBindings(project: Project, bindings: Map[String, List[ToolBinding]])

object BindingType extends Enumeration {
  type BindingType = Value
  val None, AllMessages, ToolMessagesOnly = Value
}

object ProjectBindings{

  def isFileBinding(toolBinding: ToolBinding):Boolean = {
    toolBinding.routingKey.startsWith("*.file.")
  }
  def isToolMessagesOnly(projectBindings: ProjectBindings, toolName:String):Boolean = {
    projectBindings.bindings.contains(toolName) &&
      !projectBindings.bindings(toolName).exists(binding => isFileBinding(binding))
  }

  def isNoBindings(projectBindings: ProjectBindings, toolName:String):Boolean = {
    !projectBindings.bindings.contains(toolName)
  }

  def isAllMessages(projectBindings: ProjectBindings, toolName:String):Boolean = {
    projectBindings.bindings.contains(toolName) &&
      projectBindings.bindings(toolName).exists(binding => isFileBinding(binding))
  }



}
