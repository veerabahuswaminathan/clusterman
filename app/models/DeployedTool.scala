package models

import play.api.libs.functional.syntax._
import play.api.libs.json._


case class Container(id: String, name: String)
case class Containers(containers: Seq[Container])
case class Node(id: String, name: String)
case class Replicas(requested: Int, running : Int)
case class Disk (data : Long, used : Long)

case class Label(level: String, queue: String, vhost: String, replicasMax: Int,
                 replicasMin: Int, bdType: String, scriptID: Option[String])

case class Service (containers: Seq[Container], cores: Int, disk: Disk, env: Seq[String], image: String,
                    labels: Option[Label], memory: Long, name: String, nodes: Seq[Node], replicas: Option[Replicas])

case class CpuMemResources (nanoCpus: Option[Long], memBytes: Option[Long])
case class Resources(limits: Option[CpuMemResources], reservation: Option[CpuMemResources])

// This is passed to clusterstats to deploy a new tool
case class DeployParams (name: String, image: String, env: Seq[String],
                         labels: Label, resources: Resources)

object CpuMemResources {
  implicit val cpumemresourceslimitsWrites: Writes[CpuMemResources] = (
    (JsPath \ "NanoCPUs").write[Option[Long]] and
      (JsPath \ "MemoryBytes").write[Option[Long]]
    )(unlift(CpuMemResources.unapply))
}

object Resources {
  implicit val resourcesWrites: Writes[Resources] = (
    (JsPath \ "Limits").write[Option[CpuMemResources]] and
      (JsPath \ "Reservations").write[Option[CpuMemResources]]
    )(unlift(Resources.unapply))
}

object Container {
  implicit val containerReads: Reads[Container] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "name").read[String]
    )(Container.apply _)
}

object Node {
  implicit val nodeReads: Reads[Node] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "name").read[String]
    )(Node.apply _)
}

object Replicas {
  implicit val replicasReads: Reads[Replicas] = (
    (JsPath \ "requested").read[Int] and
      (JsPath \ "running").read[Int]
    )(Replicas.apply _)
}

object Label {
  val dummyLabel = new Label("", "", "", 0, 0, "Extractor", None)
  implicit val reads: Reads[Label] = new Reads[Label] {
    override def reads(json: JsValue): JsResult[Label] = {
      for {
        level <- (json \ "bd.level").validate[String]
        queue <- (json \ "bd.rabbitmq.queue").validate[String]
        bdtype <- (json \ "bd.type").validate[String]
        vhost <- (json \ "bd.rabbitmq.vhost").validate[String]
      } yield {
        val maxValue = (json \ "bd.replicas.max")
        val max = maxValue.asOpt[Int].orElse(maxValue.asOpt[String].map(_.toInt))
        val minValue = (json \ "bd.replicas.min")
        val min = minValue.asOpt[Int].orElse(minValue.asOpt[String].map(_.toInt))
        val scriptID = (json \ "bd.toolcatalog.id").asOpt[String]
        Label(level, queue, vhost, max.get, min.get, bdtype, scriptID)
      }
    }
  }

  implicit val labelWrites = new Writes[Label] {
    def writes(label: Label): JsValue = {
      Json.obj(
        "bd.level" -> label.level,
        "bd.rabbitmq.queue" -> label.queue,
        "bd.rabbitmq.vhost" -> label.vhost,
        "bd.replicas.max" -> label.replicasMax.toString,
        "bd.replicas.min" -> label.replicasMin.toString,
        "bd.type" -> label.bdType,
        "bd.toolcatalog.id" -> label.scriptID
      )
    }
  }
}

object Disk {
  implicit val diskReads: Reads[Disk] = (
    (JsPath \ "data").read[Long] and
      (JsPath \ "used").read[Long]
    )(Disk.apply _)
}


object Service {
  implicit val serviceReads: Reads[Service] = (
    (JsPath \ "containers").read[Seq[Container]] and
      (JsPath \ "cores").read[Int] and
      (JsPath \ "disk").read[Disk] and
      (JsPath \ "env").read[Seq[String]] and
      (JsPath \ "image").read[String] and
      (JsPath \ "labels").readNullable[Label] and
      (JsPath \ "memory").read[Long] and
      (JsPath \ "name").read[String] and
      (JsPath \ "nodes").read[Seq[Node]] and
      (JsPath \ "replicas").readNullable[Replicas]
    )(Service.apply _)

}

object DeployParams {
  implicit val deployparamsWrites: Writes[DeployParams] = (
    (JsPath \ "name").write[String] and
      (JsPath \ "image").write[String] and
      (JsPath \ "env").write[Seq[String]] and
      (JsPath \ "labels").write[Label] and
      (JsPath \ "resources").write[Resources]
    )(unlift(DeployParams.unapply))
}

case class DeployedTool(id: String, service: Service)
