package models

import play.api.data.Form
import play.api.data.Forms.{list, mapping}
import play.api.data.Forms._

import scala.util.Success

/**
  *
  * @param serviceName Human friendly name
  * @param minInstances
  * @param maxInstances
  * @param memoryLimit In GBytes
  * @param cpuLimit In Cores
  */
case class DeploymentFormData(serviceName:String,
                              minInstances:Int, maxInstances:Int,
                              memoryLimit:Int, cpuLimit:Int,
                              toolType: String, queueName:String, dockerImageName:String,
                              toolId:String)

object DeploymentFormData{
  val deploymentForm = Form(
    mapping(
      "serviceName" -> text,
      "minInstances" -> number,
      "maxInstances" -> number,
      "memoryLimit" -> number,
      "cpuLimit" -> number,
      "toolType" -> text,
      "queueName" -> text,
      "dockerImageName" -> text,
      "toolId" -> text)
    (DeploymentFormData.apply)(DeploymentFormData.unapply)
  )

  def toDeploymentParms(deploymentFormData: DeploymentFormData, clusterConfig: ClusterConfig):DeployParams = {

    val labels = new Label(level = "",
      vhost = clusterConfig.vhost,
      queue = deploymentFormData.queueName,
      replicasMax = deploymentFormData.maxInstances,
      replicasMin = deploymentFormData.minInstances,
      bdType = deploymentFormData.toolType.toLowerCase(),
      scriptID = Some(deploymentFormData.toolId))

    val coresToNanos = (1e9).toInt
    val gigabytesToBytes = (1e9).toInt

    val limits = Some(new CpuMemResources(Some(deploymentFormData.cpuLimit.toLong * coresToNanos), Some(deploymentFormData.memoryLimit.toLong * gigabytesToBytes)))


    val resources = new Resources(limits, None)
    val rabbitMQConfig = ClusterConfig.getRabbitMQEntry(clusterConfig, deploymentFormData.toolType)
    val env = List(s"RABBITMQ_URI=${rabbitMQConfig.get.rabbitMQURI}")

    val serviceQueueName = deploymentFormData.queueName.replace(".", "_") // Dots are allowed in queue name, but not in service name
    val name = s"${clusterConfig.name}-${labels.bdType}-${serviceQueueName}"

    new DeployParams(name, deploymentFormData.dockerImageName, env, labels, resources)
  }

}