package models

import play.api.libs.functional.syntax._
import play.api.libs.json._
import org.joda.time.DateTime
import play.api.libs.json.JsValue

/**
  * Source code repository
  *
  * @param repType git, hg, svn
  * @param repUrl the url of the repository, for example https://opensource.ncsa.illinois.edu/stash/scm/cats/clowder.git
  */
case class Repository(repType: String, repUrl: String)
case class Process(processKey: String, mimeTypes: List[String])

 /**
  * Information about individual extractors. An extractor should set this the first time it starts up.
  *
  * Modelled after node.js package.json
  *
  * @param name lower case, no spaces, can use dashes
  * @param version the version, for example 1.3.5
  * @param updated date when this information was last updated
  * @param description short description of what the extractor does
  * @param author First Last <username@somedomain.org>
  * @param contributors list of contributors with same format as author
  * @param contexts the ids of the contexts defining the metadata uploaded by the extractors
  * @param repository source code repository
  * @param external_services external services used by the extractor
  * @param libraries libraries on which the code depends
  * @param bibtex bibtext formatted citation of relevant papers
  */
case class ExtractorInfo(
                          name: String,
                          version: String,
                          updated: Option[DateTime],
                          description: String,
                          author: String,
                          contributors: List[String],
                          contexts: JsValue,
                          repository: List[Repository],
                          process: Map[String, Process],
                          external_services: List[String],
                          dependencies: List[String],
                          libraries: List[String],
                          bibtex: List[String]
                        )



object ExtractorInfo {
  implicit val dateTimeJsReader = JodaReads.jodaDateReads("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

  implicit val repositoryReads: Reads[Repository] = (
    (JsPath \ "repType").read[String] and
      (JsPath \ "repUrl").read[String]) (Repository.apply _)

  implicit val processReads: Reads[Map[String, Process]] = new Reads[Map[String, Process]] {
    override def reads(json: JsValue): JsResult[Map[String,Process]] = {
      val obj = json.as[JsObject]

      val resultMap = obj.keys.map(key => {
         (key, new Process(key, (obj \ key).get.as[List[String]]))
      }).toMap[String, Process]

      JsSuccess(resultMap)
    }
  }

  implicit val extractorInfoReads: Reads[ExtractorInfo] = (
        (JsPath \ "name").read[String] and
          (JsPath \ "version").read[String] and
          (JsPath \ "updated").readNullable[DateTime] and
          (JsPath \ "description").read[String] and
          (JsPath \ "author").read[String] and
          (JsPath \ "contributors").read[List[String]].orElse(Reads.pure(List.empty)) and
          (JsPath \ "contexts").read[JsValue] and
          (JsPath \ "repository").read[List[Repository]] and
          (JsPath \ "process").read[Map[String, Process]] and
          (JsPath \ "external_services").read[List[String]].orElse(Reads.pure(List.empty)) and
          (JsPath \ "dependencies").read[List[String]].orElse(Reads.pure(List.empty)) and
          (JsPath \ "libraries").read[List[String]].orElse(Reads.pure(List.empty)) and
          (JsPath \ "bibtex").read[List[String]].orElse(Reads.pure(List.empty))
    )(ExtractorInfo.apply _)
}
