package models

import akka.parboiled2.RuleTrace.StringMatch
import org.joda.time.DateTime
import play.api.libs.json._
import play.api.libs.json.Json._
import play.api.libs.json.JodaWrites._

case class Params(
                   dockerfile: Option[String],
                   sampleInput:  Option[String],
                   sampleOutput: Option[String],
                   extractorName: Option[String],
                   dockerimageName: Option[String],
                   extractorDef: Option[String],
                   queueName: Option[String]
                 )

case class ToolsCatalogRecord (
                                toolId: String = "",
                                title: String,
                                shortDesc: String,
                                description: Option[String],
                                citation: Option[String],
                                video: Option[String],
                                toolType: String,
                                toolLevel: Int,

                                toolVersionId: String = "",
                                version: String,
                                url: Option[String],
                                params: Option[Params],
                                author: String = "",
                                vmImageName: Option[String],
                                status: Int,
                                interfaceLevel: Int,
                                creationDate: Option[DateTime],
                                updateDate: Option[DateTime],
                                downloads: Int = 0,
                                whatsNew: String = "", // What's New In This Version, required.
                                compatibility: Option[String], // e.g., Compatible with Polyglot 1.0, Medici 1.0.
                                reviews: Option[List[String]]
                       )


object Params{
  implicit val reads: Reads[Params] = new Reads[Params] {
    override def reads(json: JsValue): JsResult[Params] = {
      for {
        dockerfile<-(json \ "dockerfile").validateOpt[String]
        sample_input<-(json \ "sampleInput").validateOpt[String]
        sample_output<-(json \ "sampleOutput").validateOpt[String]
        extractor_name<-(json \ "extractorName").validateOpt[String]
        dockerimageName<-(json \ "dockerimageName").validateOpt[String]
        extractorDef<-(json \ "extractorDef").validateOpt[String]
        queueName<-(json\"queueName").validateOpt[String]
      } yield {
        Params(dockerfile, sample_input, sample_output, extractor_name, dockerimageName, extractorDef, queueName)
      }
    }
  }

  implicit val paramsWrite = Json.writes[Params]
}


object ToolsCatalogRecord{
  def getQueueName(tool: ToolsCatalogRecord): String = {
    val defaultQueueName = tool.version.replace(' ','.')
    tool.params match{
      case Some(params) => params.queueName getOrElse(defaultQueueName)
      case _ => defaultQueueName
    }
  }

  implicit val dateTimeJsReader = JodaReads.jodaDateReads("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

  implicit val reads: Reads[ToolsCatalogRecord] = new Reads[ToolsCatalogRecord] {
    override def reads(json: JsValue): JsResult[ToolsCatalogRecord] = {
      val toolVersion = json \ "toolVersion"
      val tool = json \ "tool"

      for {
        tool_id<-(toolVersion \ "toolId").validate[String]
        tool_version_id<-(toolVersion \ "toolVersionId").validate[String]
        version<-(toolVersion \ "version").validate[String]
        url<-(toolVersion \ "url").validateOpt[String]
        params<-(toolVersion \ "params").validateOpt[Params]
        author<-(toolVersion \ "author").validate[String]
        vmImageName<-(toolVersion \ "vmImageName").validateOpt[String]
        status<-(toolVersion \ "status").validate[Int]
        interfaceLevel<-(toolVersion \ "interfaceLevel").validate[Int]
        creationDate<-(toolVersion \ "creationDate").validateOpt[DateTime]
        updateDate<-(toolVersion \ "updateDate").validateOpt[DateTime]
        downloads<-(toolVersion \ "downloads").validate[Int]
        whatsnew<-(toolVersion \ "whatsNew").validate[String]
        compatibility<-(toolVersion \ "compatibility").validateOpt[String]
        reviews <-(toolVersion \ "reviews").validateOpt[List[String]]
        title<-(tool \ "title").validate[String]
        shortDesc<-(tool \ "shortDesc").validate[String]
        description<-(tool \ "description").validateOpt[String]
        citation<-(tool \ "citation").validateOpt[String]
        video<-(tool \ "video").validateOpt[String]
        toolType<-(tool \ "toolType").validate[String]
        toolLevel<-(tool \ "toolLevel").validate[Int]

      } yield {
        ToolsCatalogRecord(tool_id, title, shortDesc, description, citation, video, toolType, toolLevel, tool_version_id, version, url, params, author, vmImageName, status, interfaceLevel,
          creationDate, updateDate, downloads, whatsnew, compatibility, reviews)
      }
    }
  }

  implicit val toolsCatalogRecordWrite = Json.writes[ToolsCatalogRecord]
}