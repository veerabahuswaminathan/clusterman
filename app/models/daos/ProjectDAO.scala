package models.daos

import models.Project

import scala.concurrent.Future


trait ProjectDAO {
  def getProjectsForCluster(aClusterName: String): Future[List[Project]]
  def getProjectByName(aClusterName: String, projectName:String): Future[Option[Project]]
}
