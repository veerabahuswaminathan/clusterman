package models.daos
import com.google.inject.Inject
import models.Project
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.{Cursor, DefaultDB}
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ProjectDAOImpl @Inject() (val mongoDB: DefaultDB) extends ProjectDAO {
  private val collection:BSONCollection = mongoDB("projects")
  override def getProjectsForCluster(aClusterName: String): Future[List[Project]] = {
    val queryFields = BSONDocument("cluster"->aClusterName)
    collection.find(queryFields).cursor[Project]().collect[List](100, Cursor.FailOnError[List[Project]]())
  }

  override def getProjectByName(aClusterName: String, projectName: String): Future[Option[Project]] = {
    val queryFields = BSONDocument("cluster"->aClusterName, "name"->projectName)
    collection.find(queryFields).one[Project]
  }
}
