package models

import models.BindingType.BindingType
import play.api.data.Form
import play.api.data.Forms._

case class BindingFormDetail(tool: String, toolId: String, queue: String, bindingType: String, origBindingType: String)

case class BindingFormData(projectName: String, bindings: List[BindingFormDetail])

object BindingFormData {
  def generateFormDataForDeployedTools(extractors: Iterable[DeployedTool], bindings: ProjectBindings): BindingFormData = {
    val bindingDetails = extractors.map(extractor => {
      val queueName = extractor.service.labels.getOrElse(Label.dummyLabel).queue
      val toolId = extractor.service.labels.getOrElse(Label.dummyLabel).scriptID.getOrElse("")

      val bindingType = if (ProjectBindings.isNoBindings(bindings, queueName)) "None"
      else if (ProjectBindings.isAllMessages(bindings, queueName)) "All Messages"
      else "Tool Messages"

      new BindingFormDetail(extractor.service.name, toolId, queueName, bindingType, bindingType)
    }).toList


    new BindingFormData(bindings.project.name, bindingDetails)
  }

  def updatedBindings(bindingFormData: BindingFormData): List[BindingFormDetail] = {
    bindingFormData.bindings.filter(binding => !binding.origBindingType.equals(binding.bindingType))
  }

  /**
    * Map the three string vars to the correct BindingType enum
    * @param binding
    * @return
    */
  def getBindingTypeAsEnum(binding: BindingFormDetail): BindingType = {
    if (binding.bindingType.equals("All Messages")) BindingType.AllMessages
    else if (binding.bindingType.equals("Tool Messages")) BindingType.ToolMessagesOnly
    else BindingType.None
  }

  val binding = Form(
    mapping(
      "tool" -> text,
      "toolId" -> text,
      "queue" -> text,
      "bindingType" -> text,
      "origBindingType" -> text)
    (BindingFormDetail.apply)(BindingFormDetail.unapply)
  )

  val bindingForm = Form(
    mapping(
      "projectName" -> text,
      "bindings" -> list(binding.mapping))
    (BindingFormData.apply)(BindingFormData.unapply)
  )
}


