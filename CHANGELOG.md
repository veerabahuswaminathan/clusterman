# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.4.1 - 2018-08-14
### Changed

### Added

### Fixed
- Locked down the version of Java used in the generated docker image
- Improved error reporting to assist in debugging production issues
- Fixed links to make it possible to deploy behind proxy
## 0.4.0 - 2018-05-17
### Changed

### Added
- Minimally Viable Product: First release
    - Can deploy tools
    - Bind extractors to exchanges
    - LDAP Authentication
### Fixed

