# BD-Clusterman
Management tool for Brown Dog and Clowder clustsrs.


## Custom Configuration
The applications config file `application.conf` includes settings to get you
going for development and for launching the app from your IDE. It intentionallu
excludes private information that is requried for a production environment.

There is a template file containing examples of the configuration settings which
are required for running in production. You should make a copy of `custom.conf.template`
and call it `custom.conf`. This file should not be committed to the repo since it may
contain private information. It is included in `.gitignore`. Note that this file 
should `include` application.config to bring in all of the non-private config.


## Cluster Configs
Clusterman can operate on multiple deployed clusters. The configuration details for these
clusters is held in the play config file as a list of objects under the 
`clusterman.clusterConfigs` key. Here is a sample:
```
clusterman.clusterConfigs = [
  {
    name = "localhost"
    swarmURI = "http://admin:secret@localhost:9999/api"
    rabbitMQ = [
      {
        toolType = "extractor"
        rabbitMQURI: "amqp://user:pass@host:10000/extractor-host"
        secret: "Secret1"
      },
      {
        toolType: "converter"
        rabbitMQURI: "amqp://user:pass@host:10000/converter-host"
        secret: "Secret2"
      }
    ]
  }
]
```

When you launch Clusterman, there will be a drop-down menu on right side of the header where 
you can choose from the defined clusters.

## Mongo Database
Clusterman stores parameters and metrics in a Mongo Database. Set the URI for connecting
to your desired mongo instance in the `mongo.uri` key in the play config.

Make sure there is a database called `clusterman` in mongo.

## Projects
Clusterman is used to manage tool deployments, as well as bindings for tools against
_projects_ running in a cluster. Projects are defined in a mongo collection called `projects`. 

The `project` collection documents should look like:

```javascript
{
    "name" : "My project",
    "toolType" : "Extractor",
    "cluster" : "localhost"
}
```

The `toolType` property should be _Extractor_ or _Converter_.
The `cluster` property should match the name of a cluster defined in your cluster config.

## Running in Docker
To get started quickly, you can use our docker-compose stack. It will pull
the latest development image of clusterman from docker hub, and start it 
up alongside a mongo instance. 

This stack assumes that there is a `custom.conf` in the conf directory which

```bash
% docker-compose up -d
```

An instance of clusterman will be launched against port 80.


The mongo instance has a local volume to store data, and also mounts 
a sample data directory.

To load sample projects, just enter this command
```bash
% docker exec -it  mongo mongoimport --db clusterman --collection projects --drop --file /sampledata/projects.json
```

## Watchtower to Keep a CI instance up to Date
If you are running on a CI instance you will want to reflect new docker images
in the running version of the software. An optional docker compose file is provided to 
launch [WatchTower](https://github.com/v2tec/watchtower) which 
will consult dockerhub periodically and restart Clusterman whenever a new version
of the image has been pushed.

Start this up with 

```bash
% docker-compose -f watchtower-compose.yml up -d
```

## Building image in SBT 
You can build a local image from Clusterman's source code with:

```bash
sbt docker:publishLocal
```

The SBT Docker plugin is configured to read an environment variable, `configFilePath`
which should contain the path inside the container to your `custom.conf` file.

The application can be started via docker with
```bash
docker run --rm -p 9000:9000 \
           -v /clusterman:/opt/clusterman \
           -e "configFilePath=/opt/clusterman/custom.conf" \
           -d clusterman:0.0
```

This will mount `/clusterman` from your host file system. This should be the directory with 
your custom config. The folder will be mounted in the container as `/opt/clusterman`.

The `-e` argument tells docker to set the `configFilePath` environment variable and instructs
play to read your custom config.
