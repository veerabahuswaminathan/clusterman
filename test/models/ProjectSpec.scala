package models

import play.api.test.PlaySpecification
import reactivemongo.bson.BSONDocument

class ProjectSpec extends PlaySpecification{
  "Given a database record" >> {
    val doc = BSONDocument(
      "name" -> "test",
      "toolType" -> "Extractor",
      "cluster" -> "dev"
    )
    "when I read it" >> {
      val project = Project.ProjectReader.read(doc)
      "then I get a project object"   >> {
        project.name must be equalTo("test")
        project.toolType must be equalTo(ToolType.Extractor)
        project.cluster must be equalTo("dev")
      }
    }
  }

  "Given an invalid database record" >> {
    val doc = BSONDocument(
      "name" -> "test",
      "foo" -> "bar"
    )

    "When I read it from the db" >> {
      "Then I should get an exception" >> {
        Project.ProjectReader.read(doc) must throwA[Exception]
      }
    }
  }

  "Given a Project object" >> {
    val project = Project("Test", ToolType.Converter, "dev")

    "When I write it to the Database" >> {
      val doc = Project.ProjectWriter.write(project)
      "It should create a valid BSON document" >> {
        doc.getAs[String]("name") must be equalTo(Some("Test"))
        doc.getAs[String]("toolType") must be equalTo(Some("Converter"))
        doc.getAs[String]("cluster") must be equalTo(Some("dev"))
      }
    }
  }
}
