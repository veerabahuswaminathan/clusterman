package models

import play.api.libs.json.Json
import play.api.test.PlaySpecification

class BindingFormDataSpec extends PlaySpecification {
  "Given a list of deployed tools" >> {
    val deployedTools = List[DeployedTool](
      generateServiceWithQueueName("just_files", "tool0", "0"),
      generateServiceWithQueueName("files_and_tool", "tool1", "1"),
      generateServiceWithQueueName("just_tool", "tool2", "2"),
      generateServiceWithQueueName("none", "tool3", "3"),
      generateServiceWithNoLabels("no.label", "tool4")
    )

    "and a list of project bindings" >> {
      val project = Project("Test", ToolType.Converter, "dev")

      val projectBindings = ProjectBindings(project, Map[String, List[ToolBinding]](
        "just_files" -> List(
          new ToolBinding("just_files", "*.file.application.x-7z-compressed"),
          new ToolBinding("just_files", "*.file.application.x-zip")
        ),

        "files_and_tool" -> List(
          new ToolBinding("files_and_tool", "*.file.application.x-7z-compressed"),
          new ToolBinding("files_and_tool", "*.file.application.x-zip"),
          new ToolBinding("files_and_tool", "extractors.incore.geo.shp")
        ),

        "just_tool" -> List(
          new ToolBinding("just_tool", "extractors.incore.geo.shp")
        ),

      ))

      "when I display the bind tool form" >> {
        val bindingFormData = BindingFormData.generateFormDataForDeployedTools(deployedTools, projectBindings)
        "then I should have the correct binding data" >> {
          bindingFormData.projectName must be equalTo ("Test")
          bindingFormData.bindings.size must be equalTo (5)

          for (i <- 0 to 2)
            bindingFormData.bindings(i).origBindingType must be equalTo (bindingFormData.bindings(i).bindingType)

          bindingFormData.bindings(0).queue must be equalTo ("just_files")
          bindingFormData.bindings(0).bindingType must be equalTo ("All Messages")
          bindingFormData.bindings(0).tool must be equalTo ("tool0")
          bindingFormData.bindings(0).toolId must be equalTo("0")

          bindingFormData.bindings(1).queue must be equalTo ("files_and_tool")
          bindingFormData.bindings(1).bindingType must be equalTo ("All Messages")
          bindingFormData.bindings(1).tool must be equalTo ("tool1")
          bindingFormData.bindings(1).toolId must be equalTo("1")

          bindingFormData.bindings(2).queue must be equalTo ("just_tool")
          bindingFormData.bindings(2).bindingType must be equalTo ("Tool Messages")
          bindingFormData.bindings(2).tool must be equalTo ("tool2")
          bindingFormData.bindings(2).toolId must be equalTo("2")

          bindingFormData.bindings(3).queue must be equalTo ("none")
          bindingFormData.bindings(3).bindingType must be equalTo ("None")
          bindingFormData.bindings(3).tool must be equalTo ("tool3")
          bindingFormData.bindings(3).toolId must be equalTo("3")


          bindingFormData.bindings(4).queue must be equalTo("")
          bindingFormData.bindings(4).bindingType must be equalTo ("None")
          bindingFormData.bindings(4).tool must be equalTo ("tool4")
          bindingFormData.bindings(4).toolId must be equalTo("")


        }
      }

      "when I submit the form with no changes" >> {
        val bindingFormData = BindingFormData.generateFormDataForDeployedTools(deployedTools, projectBindings)
        "then the updatedBindings should be empty" >> {
          val nilUpdate = BindingFormData.updatedBindings(bindingFormData)
          nilUpdate.size must be equalTo (0)
        }
      }

      "when I submit the form with changes to the bindings" >> {
        val bindingFormData = BindingFormData.generateFormDataForDeployedTools(deployedTools, projectBindings)
        val updatedBinding = bindingFormData.bindings(0).copy(bindingType = "None")
        val updatedBindingList = List[BindingFormDetail](
          updatedBinding,
          bindingFormData.bindings(1),
          bindingFormData.bindings(2),
          bindingFormData.bindings(3))
        val updatedBindingFormData = bindingFormData.copy(bindings = updatedBindingList)
        updatedBindingFormData.bindings(0).bindingType must be equalTo ("None")

        "then the updatedBindings should report that one change" >> {
          val update = BindingFormData.updatedBindings(updatedBindingFormData)
          update.size must be equalTo (1)
          update(0).bindingType must be equalTo("None")
        }
      }

      "when I submit the form with no changes" >> {
        val bindingFormData = BindingFormData.generateFormDataForDeployedTools(deployedTools, projectBindings)
        "then the updatedBindings should be empty" >> {
          val nilUpdate = BindingFormData.updatedBindings(bindingFormData)
          nilUpdate.size must be equalTo (0)
        }
      }

      "when I submit the form with changes to the bindings" >> {
        val bindingFormData = BindingFormData.generateFormDataForDeployedTools(deployedTools, projectBindings)
        val updatedBinding = bindingFormData.bindings(0).copy(bindingType = "None")
        val updatedBindingList = List[BindingFormDetail](
          updatedBinding,
          bindingFormData.bindings(1),
          bindingFormData.bindings(2),
          bindingFormData.bindings(3))
        val updatedBindingFormData = bindingFormData.copy(bindings = updatedBindingList)
        updatedBindingFormData.bindings(0).bindingType must be equalTo ("None")

        "then the updatedBindings should report that one change" >> {
          val update = BindingFormData.updatedBindings(updatedBindingFormData)
          update.size must be equalTo (1)
          update(0).bindingType must be equalTo("None")
        }
      }
    }
  }

  "Given a binding detail for All messages" >> {
    val binding = new BindingFormDetail("myTool", "42", "myQueue", "All Messages", "")
    BindingFormData.getBindingTypeAsEnum(binding) must be equalTo(BindingType.AllMessages)
  }

  "Given a binding detail for Tool messages" >> {
    val binding = new BindingFormDetail("myTool", "42", "myQueue", "Tool Messages", "")
    BindingFormData.getBindingTypeAsEnum(binding) must be equalTo(BindingType.ToolMessagesOnly)
  }

  "Given a binding detail for No messages" >> {
    val binding = new BindingFormDetail("myTool", "42", "myQueue", "None", "")
    BindingFormData.getBindingTypeAsEnum(binding) must be equalTo(BindingType.None)
  }

  "Given a bindingDetail object" >> {
    val binding = new BindingFormDetail("myTool", "42", "myQueue", "Tool Messages", "")
    val rslt = BindingFormDetail.unapply(binding)
    rslt must beSome[(String, String, String, String, String)]

  }



  def generateServiceWithQueueName(aName: String, serviceName: String, toolId: String): DeployedTool = {
    val doc = Json.parse(
      s"""
         |{
         | "containers": [
         |   {"id": "314159",
         |    "name": "aContainer"}
         | ],
         |    "cores": 1,
         |    "disk": {
         |      "data": 2,
         |      "used": 3
         |    },
         |    "env": [ ],
         |    "image": "ncsapolyglot/converters-avconv:latest",
         |    "labels": {
         |      "bd.level": "dev",
         |      "bd.rabbitmq.queue": "$aName",
         |      "bd.rabbitmq.vhost": "dap-dev",
         |      "bd.replicas.max": "5",
         |      "bd.replicas.min": "1",
         |      "bd.type": "converter",
         |      "project": "BrownDog",
         |      "staging": "dev",
         |      "bd.toolcatalog.id": "$toolId"
         |    },
         |    "memory": 4,
         |    "name": "$serviceName",
         |    "nodes": [ ],
         |    "replicas": {
         |      "requested": 5,
         |      "running": 6
         |    }
         |  }
        """.stripMargin)

    new DeployedTool("id", Service.serviceReads.reads(doc).get)
  }

  def generateServiceWithNoLabels(aName: String, serviceName: String): DeployedTool = {
    val doc = Json.parse(
      s"""
         |{
         | "containers": [
         |   {"id": "314159",
         |    "name": "aContainer"}
         | ],
         |    "cores": 1,
         |    "disk": {
         |      "data": 2,
         |      "used": 3
         |    },
         |    "env": [ ],
         |    "image": "ncsapolyglot/converters-avconv:latest",
         |    "memory": 4,
         |    "name": "$serviceName",
         |    "nodes": [ ],
         |    "replicas": {
         |      "requested": 5,
         |      "running": 6
         |    }
         |  }
        """.stripMargin)

    new DeployedTool("id", Service.serviceReads.reads(doc).get)
  }

}
