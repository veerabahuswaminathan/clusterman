package models

import java.io.StringReader
import java.util

import com.typesafe.config.ConfigFactory
import play.api.Configuration
import play.api.test.PlaySpecification


class ClusterConfigSpec extends PlaySpecification {

  "Given a config object with a single cluster config" >> {
    val config = new Configuration(ConfigFactory.parseReader(new StringReader(
      """
        |clusterman.clusterConfigs = [
        |  {
        |    name = "localhost"
        |    swarmURI = "http://admin:secret@localhost:9999/api"
        |    vhost = "clowder-dev"
        |
        |    rabbitMQ = [
        |      {
        |        toolType = "extractor"
        |        rabbitMQURI : "amqp://user:pass@host:10000/extractor-host"
        |        secret : "Secret1"
        |      },
        |      {
        |        toolType : "converter"
        |        rabbitMQURI : "amqp://user:pass@host:10000/converter-host"
        |        secret : "Secret2"
        |      }
        |    ]
        |  }
        |]
      """.stripMargin)))
    "when I parse the config" >> {
      val cluster = ClusterConfigs.loadFromConfig(config)

      "then there should be a valid cluster config object" >> {
        cluster.clusterConfigs.size() must be equalTo (1)

        val theConfig = cluster.clusterConfigs.get(0)
        theConfig.name must be equalTo ("localhost")
        theConfig.swarmURI must be equalTo ("http://admin:secret@localhost:9999/api")

        theConfig.rabbitMQ.size() must be equalTo (2)
        val extractor = ClusterConfig.getRabbitMQEntry(theConfig, "extractor")
        extractor must not be (None)
        extractor.get.secret must be equalTo ("Secret1")
        extractor.get.rabbitMQURI must be equalTo ("amqp://user:pass@host:10000/extractor-host")
      }

      "and that config should be the default" >> {
        val defaultClusterName = ClusterConfigs.defaultConfigName(cluster)
        defaultClusterName must beSome[String]
        defaultClusterName.get must be equalTo("localhost")
      }
    }
  }

  "Given a config with multiple clusters" >> {
    val config = new Configuration(ConfigFactory.parseReader(new StringReader(
      """
        |clusterman.clusterConfigs = [
        |  {
        |    name = "localhost"
        |    swarmURI = "http://admin:secret@localhost:9999/api"
        |    vhost = "host1"
        |
        |    rabbitMQ = [ ]
        |  }
        |  {
        |    name = "dev"
        |    swarmURI = "http://admin:secret@dev.browndog:9999/api"
        |    vhost = "host2"
        |
        |    rabbitMQ = [ ]
        |  }
        |]
      """.stripMargin)))

    "when I parse the config" >> {
      val clusters = ClusterConfigs.loadFromConfig(config)

      "then I should see all of the clusters in the resulting cluster object" >> {
        clusters.clusterConfigs.size() must be equalTo (2)

        val dev = ClusterConfigs.getConfig(clusters, "dev")
        dev must not be (None)
        dev.get.name must be equalTo ("dev")
      }

      "and the first config should be the default" >> {
        val defaultClusterName = ClusterConfigs.defaultConfigName(clusters)
        defaultClusterName must beSome[String]
        defaultClusterName.get must be equalTo("localhost")
      }

    }
  }


  "Given an empty config " >> {
    val config = new Configuration(ConfigFactory.parseReader(new StringReader("clusterman.clusterConfigs = [ ] ")))

    "when I parse the config" >> {
      val clusters = ClusterConfigs.loadFromConfig(config)

      "then there should be an empty config object" >> {
        clusters.clusterConfigs.size() must be equalTo (0)
      }

      "and the default should be none" >> {
        val defaultClusterName = ClusterConfigs.defaultConfigName(clusters)
        defaultClusterName must beNone
      }

    }
  }
  "Given a Swarm Config" >> {
    "when I parse a valid uri" >> {
      "then it should succeed" >> {
        val (url, username, password) = ClusterConfig.parseURI("http://admin:secret@browndog.ncsa.illinois.edu:8080/api")
        url must be equalTo ("http://browndog.ncsa.illinois.edu:8080/api")
        username must be equalTo ("admin")
        password must be equalTo ("secret")
      }
    }
    "when I parse an invalid URI" >> {
      "then it should throw an execption" >> {
        ClusterConfig.parseURI("http://secret@browndog.ncsa.illinois.edu:8080/api") must throwA[IllegalArgumentException]
      }
    }
  }


  "Given a RabbitMQ Config" >> {
    "when I parse uri" >> {
      "then extract vhost" >> {
        val vhost = RabbitMQConfig.parseURI("amqp://user:pass@host:10000/vhost")
        vhost must be equalTo ("vhost")
      }
    }

  }

  "Exercise generated bean methods" >> {
    val configs = new ClusterConfigs()
    configs.setClusterConfigs(new util.ArrayList[ClusterConfig]())
    configs.getClusterConfigs.size() must be equalTo(0)

    val config = new ClusterConfig()
    config.setVhost("vhost")
    config.setSwarmURI("swarm")
    config.setName("name")
    config.setRabbitMQ(new util.ArrayList[RabbitMQConfig]())

    config.getName() must be equalTo("name")
    config.getSwarmURI() must be equalTo("swarm")
    config.getVhost() must be equalTo("vhost")
    config.getRabbitMQ.size() must be equalTo(0)

    val rabbitMQConfig = new RabbitMQConfig()
    rabbitMQConfig.setToolType("extractor")
    rabbitMQConfig.setSecret("shh")
    rabbitMQConfig.setRabbitMQURI("uri")

    rabbitMQConfig.getToolType must be equalTo("extractor")
    rabbitMQConfig.getSecret must be equalTo("shh")
    rabbitMQConfig.getRabbitMQURI must be equalTo("uri")

  }
}
