package models

import java.util.UUID

import com.mohiva.play.silhouette.api.LoginInfo
import org.specs2.mock.Mockito
import play.api.test.PlaySpecification

/**
  * Created by ben on 11/13/17.
  */
class UserSpec extends PlaySpecification with Mockito {
  "user model" should{
    "construct full name" can{
      "when full name provided" in {
        constructUser(Some("First"), Some("last"), Some("Mr Firstlast")).name must be equalTo(Some("Mr Firstlast"))
      }
      "when first and last provided" in {
        constructUser(Some("First"), Some("last"), None).name must be equalTo(Some("First last"))
      }
      "when just last provided" in {
        constructUser(None, Some("last"), None).name must be equalTo(Some("last"))
      }
      "when just first provided" in {
        constructUser(Some("Bob"), None , None).name must be equalTo(Some("Bob"))
      }
      "when no name provided" in{
        constructUser(None, None, None).name must be equalTo(None)
      }


    }
  }

  private def constructUser(aFirstName: Option[String], aLastName: Option[String], aFullName: Option[String]): User = {
    val loginInfo = new LoginInfo(providerID = "provider", providerKey = "key")
    new User(userID = UUID.randomUUID(),
      loginInfo = loginInfo,
      firstName = aFirstName,
      lastName = aLastName,
      fullName = aFullName,
      email = None,
      avatarURL = None,
      activated = false)
  }
}
