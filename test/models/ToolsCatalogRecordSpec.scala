package models

import org.joda.time.DateTime
import play.api.libs.json.Json
import play.api.test.PlaySpecification

class ToolsCatalogRecordSpec extends PlaySpecification {
  "Given a json document" >> {
    val doc = Json.parse(
      """
        |{
        |  "tool": {
        |    "toolId": "5a3943fa500000500010db8f",
        |    "title": "Test Tool",
        |    "url": "http://foo.com",
        |    "shortDesc": "This is just a test",
        |    "description": "Really just a test",
        |    "citation": null,
        |    "video": null,
        |    "toolType": "converter",
        |    "toolLevel": 4,
        |    "author": "guest"
        |  },
        |  "toolVersion": {
        |    "toolId": "5a3943fa500000500010db8f",
        |    "toolVersionId": "5a39440b5000004f0010db90",
        |    "version": "Test script",
        |    "url": "http://google.com",
        |    "params": {
        |      "dockerfile": "clower/der",
        |      "sampleInput": "123",
        |      "sampleOutput": "456",
        |      "extractorName": "geotiff",
        |      "dockerimageName": "ncsa/clowder-ocr:jsonld",
        |      "extractorDef": "{\"name\": \"ncsa.image.metadata\"}",
        |      "queueName": "My.queue"
        |    },
        |    "author": "guest",
        |    "vmImageName": "myVM",
        |    "status": 4,
        |    "interfaceLevel": 3,
        |    "creationDate": "2018-01-23T11:38:46.886-06:00",
        |    "updateDate": "2018-01-24T11:38:46.886-06:00",
        |    "downloads": 42,
        |    "whatsNew": "GeoTIFF is a public domain metadata standard",
        |    "compatibility": "All",
        |    "reviews": [ "****", "***"]
        |  }
        |}"""
        .stripMargin)

    "when I read it" >> {
      val record = ToolsCatalogRecord.reads.reads(doc).get

      "then the object should be populated" >> {
        record.toolId must be equalTo ("5a3943fa500000500010db8f")
        record.version must be equalTo ("Test script")
        record.url must be equalTo (Some("http://google.com"))

        record.params must beSome[Params]
        record.params.get.dockerfile must be equalTo (Some("clower/der"))
        record.params.get.sampleInput must be equalTo (Some("123"))
        record.params.get.sampleOutput must be equalTo (Some("456"))
        record.params.get.extractorName must be equalTo (Some("geotiff"))
        record.params.get.dockerimageName must be equalTo (Some("ncsa/clowder-ocr:jsonld"))
        record.params.get.extractorDef must be equalTo (Some("{\"name\": \"ncsa.image.metadata\"}"))

        record.author must be equalTo ("guest")
        record.vmImageName must be equalTo (Some("myVM"))
        record.status must be equalTo (4)
        record.interfaceLevel must be equalTo (3)

        record.creationDate.get must be equalTo (new DateTime("2018-01-23T11:38:46.886-06:00"))
        record.updateDate.get must be equalTo (new DateTime("2018-01-24T11:38:46.886-06:00"))

        record.downloads must be equalTo (42)
        record.whatsNew must be equalTo ("GeoTIFF is a public domain metadata standard")
        record.compatibility.get must be equalTo ("All")
        record.reviews.get.size must be equalTo (2)
        record.reviews.get(0) must be equalTo ("****")
      }
    }
  }
}
