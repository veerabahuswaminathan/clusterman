package models
import org.specs2.mock._
import play.api.test.PlaySpecification

class ProjectBindingsSpec extends PlaySpecification with Mockito{
  "Given a project" >> {
    val project = Project("Test", ToolType.Converter, "dev")
    val projectBindings = ProjectBindings(project, Map[String, List[ToolBinding]](
      "just_files"->List(
      new ToolBinding("just_files", "*.file.application.x-7z-compressed"),
      new ToolBinding("just_files","*.file.application.x-zip")
      ),

    "files_and_tool"->List(
      new ToolBinding("files_and_tool", "*.file.application.x-7z-compressed"),
      new ToolBinding("files_and_tool", "*.file.application.x-zip"),
      new ToolBinding("files_and_tool", "extractors.incore.geo.shp")
    ),

    "just_tool"->List(
      new ToolBinding("just_tool", "extractors.incore.geo.shp")
    ),

    ))

    "When it has file type bindings as well as tool binding" >> {
      val toolName = "files_and_tool"
      "Then it should report false for Tool Messages only" >> {
        ProjectBindings.isToolMessagesOnly(projectBindings, toolName) must beFalse
      }
      "And report false for no bindings" >> {
        ProjectBindings.isNoBindings(projectBindings, toolName) must beFalse
      }
      "And report true for all messages" >> {
        ProjectBindings.isAllMessages(projectBindings, toolName) must beTrue
      }
    }

    "When it has only file type bindings" >> {
      val toolName = "just_files"
      "Then it should report false for Tool Messages only" >> {
        ProjectBindings.isToolMessagesOnly(projectBindings, toolName) must beFalse
      }
      "And report false for no bindings" >> {
        ProjectBindings.isNoBindings(projectBindings, toolName) must beFalse
      }
      "And report true for all messages" >> {
        ProjectBindings.isAllMessages(projectBindings, toolName) must beTrue
      }
    }

    "When it has only tool bindings" >> {
      val toolName = "just_tool"
      "Then it should report true for Tool Messages only" >> {
        ProjectBindings.isToolMessagesOnly(projectBindings, toolName) must beTrue
      }
      "And report false for no bindings" >> {
        ProjectBindings.isNoBindings(projectBindings, toolName) must beFalse
      }
      "And report false for all messages" >> {
        ProjectBindings.isAllMessages(projectBindings, toolName) must beFalse
      }
    }

    "When it has no bindings" >> {
      val toolName = "not_present"
      "Then it should report false for Tool Messages only" >> {
        ProjectBindings.isToolMessagesOnly(projectBindings, toolName) must beFalse
      }
      "And report true for no bindings" >> {
        ProjectBindings.isNoBindings(projectBindings, toolName) must beTrue
      }
      "And report false for all messages" >> {
        ProjectBindings.isAllMessages(projectBindings, toolName) must beFalse
      }
    }
  }

}
