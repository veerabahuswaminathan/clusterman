package services

import com.rabbitmq.http.client.Client
import com.rabbitmq.http.client.domain.BindingInfo
import models._
import org.mockito.ArgumentCaptor
import org.specs2.mock.Mockito
import play.api.test.PlaySpecification
import _root_.services.adaptors.RabbitMQAPIAdaptor

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import org.mockito.Matchers.{eq => eqTo, _}
import org.springframework.http.HttpStatus
import org.springframework.web.client.HttpClientErrorException
import scala.concurrent.ExecutionContext.Implicits.global


class RabbitAdminServiceSpec extends PlaySpecification with Mockito {
  "RabbitMQ Admin" >> {
    val project = Project("Test", ToolType.Converter, "dev")

    def createTestBinding(destination: String, routingKey: String): BindingInfo = {
      val result = new BindingInfo()
      result.setDestination(destination)
      result.setRoutingKey(routingKey)
      result
    }

    val bindingResult = List[BindingInfo](
      createTestBinding("extractor0", "*.file.zip"),
      createTestBinding("extractor0", "*.file.gz"),
      createTestBinding("extractor1", "*.file.jpg")
    ).asJava


    def setupService(mockClient: Client): RabbitAdminService = {
      val mockAPIAdaptor = mock[RabbitMQAPIAdaptor]
      mockAPIAdaptor.createClient("http://localhost:15672/api/", "guest", "passwd").returns(mockClient)
      new RabbitAdminServiceImpl(mockAPIAdaptor)
    }

    "Connect to service" >> {
      val mockClient = mock[Client]
      val rabbit = setupService(mockClient)

      mockClient.getBindingsBySource("/", "Test").returns(bindingResult)

      val result = Await.result(rabbit.getProjectBindings("http://guest:passwd@localhost:15672", "/", project), 5 seconds)

      result.project.name must be equalTo (project.name)
      result.bindings.contains("extractor0") must beTrue
      result.bindings.get("extractor0") must beSome[List[ToolBinding]]
      result.bindings.get("extractor0").get.length must be equalTo (2)

      result.bindings.contains("extractor1") must beTrue
      result.bindings.get("extractor1") must beSome[List[ToolBinding]]
      result.bindings.get("extractor1").get.length must be equalTo (1)

    }

    "cache Admin API Clients" >> {
      val mockAPIAdaptor = mock[RabbitMQAPIAdaptor]
      val mockClient = mock[Client]
      mockAPIAdaptor.createClient("http://localhost:15672/api/", "guest", "passwd").returns(mockClient)

      val rabbit = new RabbitAdminServiceImpl(mockAPIAdaptor)
      val project = Project("Test", ToolType.Converter, "dev")


      val bindingResult = List[BindingInfo]().asJava

      mockClient.getBindingsBySource("/", "Test").returns(bindingResult)

      val result = Await.result(rabbit.getProjectBindings("http://guest:passwd@localhost:15672", "/", project), 5 seconds)
      val result2 = Await.result(rabbit.getProjectBindings("http://guest:passwd@localhost:15672", "/", project), 5 seconds)

      // Show that the two calls to getProjectBindings shared the cached call to createClient
      org.mockito.Mockito.verify(mockAPIAdaptor, org.mockito.Mockito.times(1)).createClient("http://localhost:15672/api/", "guest", "passwd")
      result.project.name must be equalTo (project.name)
      result2.project.name must be equalTo (project.name)
    }

    "Update bindings" >> {
      val fileProcess = new Process("file",List[String]("text/*", "application/json") )
      val newBindings = Map("file"->fileProcess)
      val project = Project("Test", ToolType.Converter, "dev")

      "Given a RabbitMQService" >> {
        "When I rebind with type 'all messages'" >> {
          val mockClient = mock[Client]

          val rabbit = setupService(mockClient)
          mockClient.getBindingsBySource("/", "Test").returns(bindingResult)

          val result = Await.result(rabbit.rebindProjectToQueue("http://guest:passwd@localhost:15672", "/",
            project, "extractor0", newBindings, BindingType.AllMessages), 5 seconds)

          "Then the old bindings should be removed" >> {
            org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.times(1)).unbindQueue("/",
              "extractor0", "Test", "*.file.zip")
            org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.times(1)).unbindQueue("/",
              "extractor0", "Test", "*.file.gz")

            "And the new bindings created" >> {
              org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.times(1)).bindQueue("/", "extractor0", "Test", "*.file.text.#")
              org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.times(1)).bindQueue("/", "extractor0", "Test", "*.file.application.json")
              org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.times(1)).bindQueue("/", "extractor0", "Test", "extractors.extractor0")
              result must haveClass[Success[String]]
              "And the tool binding created too" >> {
                result must beASuccessfulTry[String]
              }
            }
          }
        }

        "When I rebind with type 'tool messages'" >> {
          val mockClient = mock[Client]

          val rabbit = setupService(mockClient)
          mockClient.getBindingsBySource("/", "Test").returns(bindingResult)

          val result = Await.result(rabbit.rebindProjectToQueue("http://guest:passwd@localhost:15672", "/",
            project, "extractor0", newBindings, BindingType.ToolMessagesOnly), 5 seconds)

          "Then the old bindings should be removed" >> {
            org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.times(1)).unbindQueue("/",
              "extractor0", "Test", "*.file.zip")
            org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.times(1)).unbindQueue("/",
              "extractor0", "Test", "*.file.gz")

            "And the new bindings created" >> {
              org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.times(1)).bindQueue("/", "extractor0", "Test", "extractors.extractor0")
              org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.never()).bindQueue(eqTo("/"), eqTo("extractor0"), eqTo("Test"), startWith("*."))
              result must haveClass[Success[String]]
              "And the tool binding created too" >> {
                result must beASuccessfulTry[String]
              }
            }
          }
        }

        "When I rebind with type 'None'" >> {
          val mockClient = mock[Client]

          val rabbit = setupService(mockClient)
          mockClient.getBindingsBySource("/", "Test").returns(bindingResult)

          val result = Await.result(rabbit.rebindProjectToQueue("http://guest:passwd@localhost:15672", "/",
            project, "extractor0", newBindings, BindingType.None), 5 seconds)

          "Then the old bindings should be removed" >> {
            org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.times(1)).unbindQueue("/",
              "extractor0", "Test", "*.file.zip")
            org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.times(1)).unbindQueue("/",
              "extractor0", "Test", "*.file.gz")

            "And no new bindings created" >> {
              org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.never()).bindQueue("/", "extractor0", "Test", "extractors.extractor0")
              org.mockito.Mockito.verify(mockClient, org.mockito.Mockito.never()).bindQueue(eqTo("/"), eqTo("extractor0"), eqTo("Test"), startWith("*."))
              result must haveClass[Success[String]]
              "And the tool binding created too" >> {
                result must beASuccessfulTry[String]
              }
            }
          }
        }
        "When the unbind operation throws an exception" >> {
          val mockClient = mock[Client]
          val rabbit = setupService(mockClient)
          mockClient.getBindingsBySource("/", "Test").returns(bindingResult)
          mockClient.unbindQueue("/", "extractor0", "Test", "*.file.gz").throws(new HttpClientErrorException(HttpStatus.GONE))

          val r = rabbit.rebindProjectToQueue("http://guest:passwd@localhost:15672", "/",
            project, "extractor0", newBindings, BindingType.AllMessages)
          val result = Await.result(r, 5 seconds)

          "Then the rebind operation should fail" >> {
            result must beAFailedTry
          }
        }

        "When bind operation throws exception" >> {
          val mockClient = mock[Client]
          val rabbit = setupService(mockClient)
          mockClient.getBindingsBySource("/", "Test").returns(bindingResult)
          mockClient.bindQueue("/", "extractor0", "Test", "*.file.application.json").throws(new HttpClientErrorException(HttpStatus.GONE))

          val r = rabbit.rebindProjectToQueue("http://guest:passwd@localhost:15672", "/",
            project, "extractor0", newBindings, BindingType.AllMessages)

          val result = Await.result(r, 5 seconds)

          "Then the rebind operation should fail" >> {
            result must beAFailedTry
          }
        }

      "When bind operation throws exception when binding tools" >> {
        val mockClient = mock[Client]
        val rabbit = setupService(mockClient)
        mockClient.getBindingsBySource("/", "Test").returns(bindingResult)
        mockClient.bindQueue("/", "extractor0", "Test", "extractors.extractor0").throws(new HttpClientErrorException(HttpStatus.GONE))

        val r = rabbit.rebindProjectToQueue("http://guest:passwd@localhost:15672", "/",
          project, "extractor0", newBindings, BindingType.ToolMessagesOnly)

        val result = Await.result(r, 5 seconds)

        "Then the rebind operation should fail" >> {
          result must beAFailedTry
        }
      }
    }


    "When error unbinding existing queues" >> {
        "then the rebind operation should fail" >> {
          val mockAPIAdaptor = mock[RabbitMQAPIAdaptor]
          val mockClient = mock[Client]
          mockAPIAdaptor.createClient("http://localhost:15672/api/", "guest", "passwd").returns(mockClient)

          val rabbit = new RabbitAdminServiceImpl(mockAPIAdaptor)

          mockClient.getBindingsBySource("/", "Test").returns(bindingResult)
          mockClient.unbindQueue("/",
            "extractor0", "Test", "*.file.zip").throws(new HttpClientErrorException(HttpStatus.GONE))

          val result = Await.result(rabbit.rebindProjectToQueue("http://guest:passwd@localhost:15672", "/",
            project, "extractor0", newBindings, BindingType.AllMessages), 5 seconds)

          result must beAFailedTry

        }
      }

      "When there are no existing bindings for the queue">> {
        "Then the rebind operations should skip the initial unbind" >>{
          val mockAPIAdaptor = mock[RabbitMQAPIAdaptor]
          val mockClient = mock[Client]
          mockAPIAdaptor.createClient("http://localhost:15672/api/", "guest", "passwd").returns(mockClient)

          val rabbit = new RabbitAdminServiceImpl(mockAPIAdaptor)
          mockClient.getBindingsBySource("/", "Test").returns(bindingResult)

          val result = Await.result(rabbit.rebindProjectToQueue("http://guest:passwd@localhost:15672", "/",
            project, "QueueWithThisName", newBindings, BindingType.AllMessages), 5 seconds)

          result must beASuccessfulTry[String]

        }
      }

    }
  }

}
