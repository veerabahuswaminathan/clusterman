package controllers

import java.util.UUID

import com.google.inject.AbstractModule
import com.mohiva.play.silhouette.api.{Environment, LoginInfo}
import com.mohiva.play.silhouette.test._
import models.{ClusterConfig, ClusterConfigs, Project, User}
import models.daos.{ProjectDAO, ProjectDAOImpl}
import net.codingwell.scalaguice.ScalaModule
import org.specs2.mock.Mockito
import org.specs2.specification.Scope
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.CSRFTokenHelper._
import play.api.test.{FakeRequest, PlaySpecification, WithApplication}
import reactivemongo.api.DefaultDB
import services.RabbitAdminService
import services.adaptors.{RabbitMQAPIAdaptor, RabbitMQAPIAdaptorImpl}
import utils.auth.DefaultEnv

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
  * Test case for the [[controllers.ApplicationController]] class.
  */
class ApplicationControllerSpec extends PlaySpecification with Mockito {
  val mockProjectDAO = mock[ProjectDAO]

  "The `index` action" should {
    "redirect to login page if user is unauthorized" in  new Context {
      mockProjectDAO.getProjectsForCluster(anyString) returns Future(List[Project]())

      new WithApplication(application) {
        val Some(redirectResultFut) = route(app, FakeRequest(routes.ApplicationController.index())
          .withAuthenticator[DefaultEnv](LoginInfo("invalid", "invalid"))
        )

        val redirectResult = Await.result(redirectResultFut, 10 seconds)

        redirectResult.header.status must be equalTo SEE_OTHER

        val redirectURL = redirectResult.header.headers.getOrElse("Location", "")
        redirectURL must contain(routes.SignInController.view().toString)

        val Some(unauthorizedResult) = route(app, addCSRFToken(FakeRequest(GET, redirectURL)))

        status(unauthorizedResult) must be equalTo OK
        contentType(unauthorizedResult) must beSome("text/html")
        contentAsString(unauthorizedResult) must contain("ClusterMan - Sign In")
      }
    }

    "return 200 if user is authorized" in new Context {
      new WithApplication(application) {
        val Some(result) = route(app, addCSRFToken(FakeRequest(GET, "/?cluster=foo")
          .withAuthenticator[DefaultEnv](identity.loginInfo))
        )

        status(result) must beEqualTo(OK)
      }
    }


  "display the default cluster when no cluster provided" in new Context {
    new WithApplication(application) {
      mockProjectDAO.getProjectsForCluster("") returns Future(List[Project]())

      val Some(result) = route(app, addCSRFToken(FakeRequest(GET, "/")
        .withAuthenticator[DefaultEnv](identity.loginInfo))
      )

      status(result) must beEqualTo(OK)
      there was one(mockProjectDAO).getProjectsForCluster("myCluster")

    }
  }

    "display the selected cluster when provided" in new Context {
      new WithApplication(application) {

        mockProjectDAO.getProjectsForCluster("yourCluster") returns Future(List[Project]())
        val Some(result) = route(app, addCSRFToken(FakeRequest(GET, "/?cluster=yourCluster")
          .withAuthenticator[DefaultEnv](identity.loginInfo))
        )
        status(result) must beEqualTo(OK)
        there was one(mockProjectDAO).getProjectsForCluster("yourCluster")
      }
    }
}


"the 'signout' action" should {
    "redirect browser to home" in new Context{
      new WithApplication(application) {
        val Some(result2) = route(app, addCSRFToken(FakeRequest(routes.ApplicationController.signOut())
          .withAuthenticator[DefaultEnv](identity.loginInfo))
        )
        status(result2) must beEqualTo(SEE_OTHER)
      }


    }

  }

  /**
    * The context.
    */
  trait Context extends Scope {

    /**
      * A fake Guice module.
      */
    val mockDB = mock[DefaultDB]
    val mockRabbitAdminService = mock[RabbitAdminService]
    val clusters = new java.util.ArrayList[ClusterConfig]()
    clusters.add(new ClusterConfig("myCluster", "http://foo.com", "/", null))


    class FakeModule extends AbstractModule with ScalaModule {
      def configure() = {
        bind[Environment[DefaultEnv]].toInstance(env)
        bind[DefaultDB].toInstance(mockDB)
        bind[ProjectDAO].toInstance(mockProjectDAO)
        bind[RabbitAdminService].toInstance(mockRabbitAdminService)
        bind[ClusterConfigs].toInstance(new ClusterConfigs(clusters))
      }
    }

    /**
      * An identity.
      */
    val identity = User(
      userID = UUID.randomUUID(),
      loginInfo = LoginInfo("facebook", "user@facebook.com"),
      firstName = None,
      lastName = None,
      fullName = None,
      email = None,
      avatarURL = None,
      activated = true
    )

    /**
      * A Silhouette fake environment.
      */
    implicit val env: Environment[DefaultEnv] = new FakeEnvironment[DefaultEnv](Seq(identity.loginInfo -> identity))

    /**
      * The application.
      */
    lazy val application = new GuiceApplicationBuilder()
      .overrides(new FakeModule)
      .build()
  }
}
