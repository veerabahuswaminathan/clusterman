package controllers

import java.util
import java.util.UUID

import _root_.services.{ClusterstatsService, RabbitAdminService, ToolsCatalogService}
import com.google.inject.AbstractModule
import com.mohiva.play.silhouette.api.{Environment, LoginInfo}
import com.mohiva.play.silhouette.test._
import models._
import models.daos.ProjectDAO
import net.codingwell.scalaguice.ScalaModule
import org.mockito.Matchers.{eq => eqTo}
import org.specs2.mock.Mockito
import org.specs2.specification.Scope
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.Result
import play.api.test.CSRFTokenHelper._
import play.api.test.{FakeRequest, PlaySpecification, WithApplication}
import reactivemongo.api.DefaultDB
import utils.auth.DefaultEnv

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success} // Oops there is a services package inside models which clashes

class ToolsControllerSpec extends PlaySpecification with Mockito {

  "Given there is a cluster" >> {
    val doc = Json.parse(
      """
        |[
        |{
        |  "tool": {
        |    "toolId": "5a3943fa500000500010db8f",
        |    "title": "Test Tool",
        |    "url": "http://foo.com",
        |    "shortDesc": "This is just a test",
        |    "description": "Really just a test",
        |    "citation": null,
        |    "video": null,
        |    "toolType": "converter",
        |    "toolLevel": 4,
        |    "author": "guest"
        |  },
        |  "toolVersion": {
        |    "toolId": "5a3943fa500000500010db8f",
        |    "toolVersionId": "5a39440b5000004f0010db90",
        |    "version": "Test script",
        |    "url": "",
        |    "params": {
        |      "dockerfile": "http://docker.file",
        |      "sampleInput": "http://example-input",
        |      "sampleOutput": "http://example-output",
        |      "extractorName": "My.queue",
        |      "dockerimageName": "Image",
        |      "extractorDef": "",
        |      "queueName": "My.queue"
        |    },
        |    "author": "guest",
        |    "vmImageName": "",
        |    "status": 4,
        |    "interfaceLevel": 4,
        |    "creationDate": "2017-12-19T10:53:31.701-06:00",
        |    "updateDate": "2017-12-19T10:53:31.701-06:00",
        |    "downloads": 0,
        |    "whatsNew": "This is s a test",
        |    "compatibility": "",
        |    "reviews": null
        |  }
        |}
        |]
      """.stripMargin)
    val toolsCatalogRecords = doc.as[List[ToolsCatalogRecord]]

    "When I click on the deploy button" >> {
      "Then I should see the deploy page" >> new Context {
        val mockToolsCatalogService = mock[ToolsCatalogService]
        val mockClusterStatsService = mock[ClusterstatsService]


        mockToolsCatalogService.getExtractors(any[WSClient],eqTo("http://localhost:9000"))
          .returns(Future(toolsCatalogRecords))
        mockToolsCatalogService.getConverters(any[WSClient],eqTo("http://localhost:9000"))
          .returns(Future(toolsCatalogRecords))


        new WithApplication(app(mockToolsCatalogService, mockClusterStatsService)) {
          val Some(resultFut) = route(app,
            addCSRFToken(FakeRequest(routes.ToolsController.chooseTool("myCluster"))
              .withAuthenticator[DefaultEnv](identity.loginInfo))
          )

          val result: Result = Await.result(resultFut, 5 seconds)
          result.header.status should be equalTo (OK)

          there was one(mockToolsCatalogService).getExtractors(any[WSClient], eqTo("http://localhost:9000"))
        }
      }
    }
  }

  "Given I have selected a tool to deploy" >>{
    val doc = Json.parse(
      """
        |{
        |  "tool": {
        |    "toolId": "5a3943fa500000500010db8f",
        |    "title": "Test Tool",
        |    "url": "http://foo.com",
        |    "shortDesc": "This is just a test",
        |    "description": "Really just a test",
        |    "citation": null,
        |    "video": null,
        |    "toolType": "converter",
        |    "toolLevel": 4,
        |    "author": "guest"
        |  },
        |  "toolVersion": {
        |    "toolId": "5a3943fa500000500010db8f",
        |    "toolVersionId": "5a39440b5000004f0010db90",
        |    "version": "Test script",
        |    "url": "",
        |    "params": {
        |      "dockerfile": "http://docker.file",
        |      "sampleInput": "http://example-input",
        |      "sampleOutput": "http://example-output",
        |      "extractorName": "My.queue",
        |      "dockerimageName": "Image",
        |      "extractorDef": "",
        |      "queueName": "My.queue"
        |    },
        |    "author": "guest",
        |    "vmImageName": "",
        |    "status": 4,
        |    "interfaceLevel": 4,
        |    "creationDate": "2017-12-19T10:53:31.701-06:00",
        |    "updateDate": "2017-12-19T10:53:31.701-06:00",
        |    "downloads": 0,
        |    "whatsNew": "This is s a test",
        |    "compatibility": "",
        |    "reviews": null
        |  }
        |}
      """.stripMargin)

    "When I click on the 'Next' button " >> {
        "Then I should see a form with the deployment parameters populated" >> new Context{
          val mockToolsCatalogService = mock[ToolsCatalogService]
          val mockClusterStatsService = mock[ClusterstatsService]

          mockToolsCatalogService.getTool(any[WSClient], eqTo("http://localhost:9000"), eqTo("314159"))
            .returns(Future(Some(doc.as[ToolsCatalogRecord])))

          new WithApplication(app(mockToolsCatalogService, mockClusterStatsService)) {
            val Some(resultFut) = route(app,
              addCSRFToken(FakeRequest(routes.ToolsController.enterDeployParameters("myCluster", "314159"))
                .withAuthenticator[DefaultEnv](identity.loginInfo))
            )

            val result: Result = Await.result(resultFut, 5 seconds)
            result.header.status should be equalTo (OK)
            there was one(mockToolsCatalogService).getTool(any[WSClient], eqTo("http://localhost:9000"), eqTo("314159"))
          }
      }
    }
  }

  "Given I have selected a tool to deploy" >> {
    "And the tool is missing a docker image" >> {
      val doc = Json.parse(
        """
        |{
        |  "tool": {
        |    "toolId": "5a3943fa500000500010db8f",
        |    "title": "Test Tool",
        |    "url": "http://foo.com",
        |    "shortDesc": "This is just a test",
        |    "description": "Really just a test",
        |    "citation": null,
        |    "video": null,
        |    "toolType": "converter",
        |    "toolLevel": 4,
        |    "author": "guest"
        |  },
        |  "toolVersion": {
        |    "toolId": "5a3943fa500000500010db8f",
        |    "toolVersionId": "5a39440b5000004f0010db90",
        |    "version": "Test script",
        |    "url": "",
        |    "params": {
        |      "dockerfile": "http://docker.file",
        |      "sampleInput": "http://example-input",
        |      "sampleOutput": "http://example-output",
        |      "extractorName": "My.queue",
        |      "extractorDef": "",
        |      "queueName": "My.queue"
        |    },
        |    "author": "guest",
        |    "vmImageName": "",
        |    "status": 4,
        |    "interfaceLevel": 4,
        |    "creationDate": "2017-12-19T10:53:31.701-06:00",
        |    "updateDate": "2017-12-19T10:53:31.701-06:00",
        |    "downloads": 0,
        |    "whatsNew": "This is s a test",
        |    "compatibility": "",
        |    "reviews": null
        |  }
        |}
      """.stripMargin)
      "When I click on the 'Next' button " >> {
        "Then I should see a form with the deployment parameters populated" >> new Context{
        val mockToolsCatalogService = mock[ToolsCatalogService]
        val mockClusterStatsService = mock[ClusterstatsService]

        mockToolsCatalogService.getTool(any[WSClient], eqTo("http://localhost:9000"), eqTo("314159"))
          .returns(Future(Some(doc.as[ToolsCatalogRecord])))

        new WithApplication(app(mockToolsCatalogService, mockClusterStatsService)) {
          val Some(resultFut) = route(app,
            addCSRFToken(FakeRequest(routes.
              ToolsController.enterDeployParameters("myCluster", "314159"))
              .withAuthenticator[DefaultEnv]
              (identity.loginInfo))
          )
          val result: Result = Await.result(resultFut, 5 seconds)
          result.header.status should be equalTo (BAD_REQUEST)
          there was one(mockToolsCatalogService).getTool(any[WSClient], eqTo("http://localhost:9000"), eqTo("314159"))
        }
      }
    }
  }
    }

  "Given I have selected a tool to deploy" >> {
    "And the tool is missing a parameters " >> {
      val doc = Json.parse(
        """
          |{
          |  "tool": {
          |    "toolId": "5a3943fa500000500010db8f",
          |    "title": "Test Tool",
          |    "url": "http://foo.com",
          |    "shortDesc": "This is just a test",
          |    "description": "Really just a test",
          |    "citation": null,
          |    "video": null,
          |    "toolType": "converter",
          |    "toolLevel": 4,
          |    "author": "guest"
          |  },
          |  "toolVersion": {
          |    "toolId": "5a3943fa500000500010db8f",
          |    "toolVersionId": "5a39440b5000004f0010db90",
          |    "version": "Test script",
          |    "url": "",
          |    "author": "guest",
          |    "vmImageName": "",
          |    "status": 4,
          |    "interfaceLevel": 4,
          |    "creationDate": "2017-12-19T10:53:31.701-06:00",
          |    "updateDate": "2017-12-19T10:53:31.701-06:00",
          |    "downloads": 0,
          |    "whatsNew": "This is s a test",
          |    "compatibility": "",
          |    "reviews": null
          |  }
          |}
        """.stripMargin)
      "When I click on the 'Next' button " >> {
        "Then I should see a form with the deployment parameters populated" >> new Context{
          val mockToolsCatalogService = mock[ToolsCatalogService]
          val mockClusterStatsService = mock[ClusterstatsService]

          mockToolsCatalogService.getTool(any[WSClient], eqTo("http://localhost:9000"), eqTo("314159"))
            .returns(Future(Some(doc.as[ToolsCatalogRecord])))

          new WithApplication(app(mockToolsCatalogService, mockClusterStatsService)) {
            val Some(resultFut) = route(app,
              addCSRFToken(FakeRequest(routes.
                ToolsController.enterDeployParameters("myCluster", "314159"))
                .withAuthenticator[DefaultEnv]
                (identity.loginInfo))
            )
            val result: Result = Await.result(resultFut, 5 seconds)
            result.header.status should be equalTo (BAD_REQUEST)
            there was one(mockToolsCatalogService).getTool(any[WSClient], eqTo("http://localhost:9000"), eqTo("314159"))
          }
        }
      }
    }
  }


  "Given I have selected a tool with an invalid description in tools catalog" >> {
    "When I click on the 'Next' button " >> {
      "Then I should see an BadRequest result" >> new Context {
        val mockToolsCatalogService = mock[ToolsCatalogService]
        val mockClusterStatsService = mock[ClusterstatsService]

        mockToolsCatalogService.getTool(any[WSClient], eqTo("http://localhost:9000"), eqTo("314159"))
          .returns(Future(None))

        new WithApplication(app(mockToolsCatalogService, mockClusterStatsService)) {
          val Some(resultFut) = route(app,
            addCSRFToken(FakeRequest(routes.ToolsController.enterDeployParameters("myCluster", "314159"))
              .withAuthenticator[DefaultEnv](identity.loginInfo))
          )

          val result: Result = Await.result(resultFut, 5 seconds)
          result.header.status should be equalTo (BAD_REQUEST)
          there was one(mockToolsCatalogService).getTool(any[WSClient], eqTo("http://localhost:9000"), eqTo("314159"))
        }
      }
    }
  }

 "Given I have entered correct deployment parameters" >> {
   "When I click 'deploy' button" >> {
     "Then Clusterman should submit the request to clusterstats" >> new Context {
       val mockToolsCatalogService = mock[ToolsCatalogService]
       val mockClusterStatsService = mock[ClusterstatsService]

       val captor = capture[DeployParams]

       mockClusterStatsService.deploy(any[WSClient], eqTo("http://user:pass@localhost:8888"), any[DeployParams]).returns(Future(Success("ok!")))

       new WithApplication(app(mockToolsCatalogService, mockClusterStatsService)) {
         val Some(resultFut) = route(app,
           addCSRFToken(FakeRequest(routes.ToolsController.deployToolToCluster("myCluster", "314159"))
             .withJsonBody(Json.parse(
             """
               |{
               |      "serviceName" : "myService",
               |      "minInstances" : 1,
               |      "maxInstances" : 3,
               |      "memoryLimit" : 6,
               |      "cpuLimit" : 4,
               |      "toolType" : "extractor",
               |      "queueName" : "my.queue",
               |      "dockerImageName" : "clow/der",
               |      "toolId" : "314159"
               | }
             """.stripMargin))
             .withAuthenticator[DefaultEnv](identity.loginInfo))
         )

         val result: Result = Await.result(resultFut, 5 seconds)
         result.header.status should be equalTo (OK)
         there was one(mockClusterStatsService).deploy(any[WSClient],
           eqTo("http://user:pass@localhost:8888"),
           captor.capture)

         val capturedDeployParams = captor.value
         capturedDeployParams.name must be equalTo("myCluster-extractor-my_queue")
         capturedDeployParams.image must be equalTo("clow/der")
         capturedDeployParams.resources.limits must beSome
         capturedDeployParams.resources.limits.get.memBytes must beSome
         capturedDeployParams.resources.limits.get.memBytes must be equalTo(Some(6e9.toLong))

         capturedDeployParams.resources.limits.get.nanoCpus must beSome
         capturedDeployParams.resources.limits.get.nanoCpus must be equalTo(Some(4e9.toLong))

         capturedDeployParams.labels.scriptID must be equalTo(Some("314159"))
         capturedDeployParams.labels.queue must be equalTo("my.queue")
         capturedDeployParams.labels.vhost must be equalTo("clowder-dev")
         capturedDeployParams.labels.bdType must be equalTo("extractor")
         capturedDeployParams.labels.replicasMin must be equalTo(1)
         capturedDeployParams.labels.replicasMax must be equalTo(3)

         capturedDeployParams.env.size must be equalTo(1)
         capturedDeployParams.env.head must be equalTo("RABBITMQ_URI=http://user:pass@host:10000")
       }
     }
   }
 }

  "Given I have incorrectly populated the deploy form" >> {
    "And I have selected an unknown cluster" >> {
      "When I click deploy" >> {
        "Then I should see the error of my ways" >> new Context {
          new WithApplication(app(mock[ToolsCatalogService], mock[ClusterstatsService])) {
            val Some(resultFut) = route(app,
              addCSRFToken(FakeRequest(routes.ToolsController.deployToolToCluster("BADCLUSTER", "314159"))
                .withJsonBody(Json.parse(
                  """
                  |{
                  |      "serviceName" : "myService",
                  |      "minInstances" : 1,
                  |      "maxInstances" : 3,
                  |      "memoryLimit" : 6,
                  |      "cpuLimit" : 4,
                  |      "toolType" : "extractor",
                  |      "queueName" : "my.queue",
                  |      "dockerImageName" : "clow/der",
                  |      "toolId" : "314159"
                  | }
                """.stripMargin))
              .withAuthenticator[
                DefaultEnv](identity.loginInfo))
          )

          val

          result: Result = Await.result(resultFut, 5 seconds)
          result.header.status should be equalTo (BAD_REQUEST)
        }
        }
      }
    }
  }

  "Given I have incorrectly populated the deploy form" >> {
      "When I click deploy" >> {
        "Then I should see the error of my ways" >> new Context {
          new WithApplication(app(mock[ToolsCatalogService], mock[ClusterstatsService])) {
            val Some(resultFut) = route(app,
              addCSRFToken(FakeRequest(routes.ToolsController.deployToolToCluster("myCluster", "314159"))
                .withJsonBody(Json.parse(
                  """
                  |{
                  |      "serviceName" : "myService",
                  |      "toolId" : "314159"
                  | }
                """.
                    stripMargin))
              .withAuthenticator[DefaultEnv](identity.loginInfo))
          )

          val  result: Result = Await.result(resultFut, 5 seconds)
          result.header.status should be equalTo (
            BAD_REQUEST)
        }
        }
    }
    }


  "Given I have entered correct deployment parameters" >> {
    "And the parameters will cause an error in clusterstats" >> {
      "When I click 'deploy' button" >> {
        "Then I should see an error" >> new Context {
          val mockToolsCatalogService = mock[ToolsCatalogService]
          val mockClusterStatsService = mock[ClusterstatsService]

          val captor = capture[DeployParams]

          mockClusterStatsService.deploy(any[WSClient], eqTo("http://user:pass@localhost:8888"), any[DeployParams]).returns(Future(Failure(new Exception("boo!"))))

          new WithApplication(app(mockToolsCatalogService, mockClusterStatsService)) {
            val Some(resultFut) = route(app,
              addCSRFToken(FakeRequest(routes.ToolsController.deployToolToCluster("myCluster", "314159"))
                .withJsonBody(Json.parse(
                  """
                  |{
                  |      "serviceName" : "myService",
                  |      "minInstances" : 1,
                  |      "maxInstances" : 3,
                  |      "memoryLimit" : 6,
                  |      "cpuLimit" : 4,
                  |      "toolType" : "extractor",
                  |      "queueName" : "my.queue",
                  |      "dockerImageName" : "clow/der",
                  |      "toolId" : "314159"
                  | }
                """.stripMargin))
              .withAuthenticator[
                DefaultEnv](identity.loginInfo))
          )

          val

          result: Result = Await.result(resultFut, 5 seconds)
          result.header.status should be equalTo (INTERNAL_SERVER_ERROR)
          there was one(mockClusterStatsService).deploy(any[WSClient],
            eqTo("http://user:pass@localhost:8888"),
            any[DeployParams])
          }
        }
      }
    }
  }

  /**
    * The context.
    */
  trait Context extends Scope {
    val mockProjectDAO = mock[ProjectDAO]

    /**
      * A fake Guice module.
      */
    class FakeModule(toolsCatalog: ToolsCatalogService, clusterstatsService: ClusterstatsService) extends AbstractModule with ScalaModule {
      val mockDB = mock[DefaultDB]
      val mockRabbit = mock[RabbitAdminService]

      def configure() = {
        bind[Environment[DefaultEnv]].toInstance(env)
        bind[DefaultDB].toInstance(mockDB)
        bind[ProjectDAO].toInstance(mockProjectDAO)
        bind[ClusterstatsService].toInstance(clusterstatsService)
        bind[RabbitAdminService].toInstance(mockRabbit)
        bind[ToolsCatalogService].toInstance(toolsCatalog)
      }
    }

    /**
      * An identity.
      */
    val identity = User(
      userID = UUID.randomUUID(),
      loginInfo = LoginInfo("facebook", "user@facebook.com"),
      firstName = None,
      lastName = None,
      fullName = None,
      email = None,
      avatarURL = None,
      activated = true
    )

    /**
      * A Silhouette fake environment.
      */
    implicit val env: Environment[DefaultEnv] = new FakeEnvironment[DefaultEnv](Seq(identity.loginInfo -> identity))


    // Create my own cluster config
    val clusterMap = new java.util.HashMap[String, Object]()
    clusterMap.put("name", "myCluster")
    clusterMap.put("swarmURI", "http://user:pass@localhost:8888")
    clusterMap.put("vhost", "clowder-dev")

    val rabbitConfig = new util.ArrayList[Object]()
    val extractorConfig = new util.HashMap[String, Object]()

    extractorConfig.put("toolType", "Extractor")
    extractorConfig.put("rabbitMQURI", "http://user:pass@host:10000")
    extractorConfig.put("secret", "shh")

    rabbitConfig.add(extractorConfig)
    clusterMap.put("rabbitMQ", rabbitConfig)

    val clusterConfigList = new java.util.ArrayList[java.util.Map[String, Object]]()
    clusterConfigList.add(clusterMap)

    def app(toolsCatalog: ToolsCatalogService, clusterstatsService: ClusterstatsService): Application = {
      new GuiceApplicationBuilder()
        .overrides(new FakeModule(toolsCatalog, clusterstatsService))
        .configure(("clusterman.clusterConfigs", clusterConfigList), ("toolscatalog.uri", "http://localhost:9000"))
        .build()
    }
  }
}
