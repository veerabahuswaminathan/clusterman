import com.typesafe.sbt.packager.docker.{Cmd, ExecCmd}
import sbt.{TestFrameworks, Tests}

name := "clusterman"

version := "0.0"

lazy val `bd_clusterman` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.2"


libraryDependencies ++= Seq(
  "com.mohiva" %% "play-silhouette" % "5.0.0",
  "com.mohiva" %% "play-silhouette-password-bcrypt" % "5.0.0",
  "com.mohiva" %% "play-silhouette-persistence" % "5.0.0",
  "com.mohiva" %% "play-silhouette-crypto-jca" % "5.0.0",
  "org.webjars" %% "webjars-play" % "2.6.1",
  "org.webjars" % "bootstrap" % "3.3.7-1" exclude("org.webjars", "jquery"),
  "org.webjars" % "jquery" % "3.2.1",
  "org.reactivemongo" % "reactivemongo_2.12" % "0.12.7",
  "net.codingwell" %% "scala-guice" % "4.1.0",
  "com.iheart" %% "ficus" % "1.4.1",
  "com.typesafe.play" %% "play-mailer" % "6.0.1",
  "com.typesafe.play" %% "play-mailer-guice" % "6.0.1",
  "com.typesafe.play" % "play-json-joda_2.12" % "2.6.0",
  "com.enragedginger" %% "akka-quartz-scheduler" % "1.6.1-akka-2.5.x",
  "com.adrianhurt" %% "play-bootstrap" % "1.2-P26-B3",
  "com.rabbitmq" % "http-client" % "2.0.1.RELEASE",
  "com.mohiva" %% "play-silhouette-testkit" % "5.0.0" % "test",
  "com.unboundid" % "unboundid-ldapsdk" % "4.0.1",
  specs2 % Test,
  ehcache,
  guice,
  filters
)

dockerBaseImage:="openjdk:8-jdk"

dockerCommands ++= Seq(
  Cmd("ENV", "configFilePath", "/opt/clusterman/prod.conf"), // This will be overridden when running!
  // This is the entrypoint where we can run the application against different environments
  ExecCmd("ENTRYPOINT", "sh", "-c", "bin/" + s"${executableScriptName.value}" + " -Dconfig.file=$configFilePath")
)

libraryDependencies += "org.pegdown" % "pegdown" % "1.6.0" % Test

//unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
resolvers += "Atlassian Releases" at "https://maven.atlassian.com/public/"

testOptions in Test ++= Seq(Tests.Argument(TestFrameworks.ScalaTest, "-h", "target/test-reports/html"))

coverageExcludedPackages := "<empty>;Reverse.*;controllers.javascript.*;models\\.data\\..*;models.daos.*;modules.*;router.*; utils.auth.*;utils.Filters; views.*;services.adaptors.*"

enablePlugins(DockerPlugin)


